import {od6sutilities} from "./utilities.js";

export class OD6SChat {

    static async chatContextMenu(html, options) {
        let message = (li) => {
            return game.messages.get(li.attr("data-message-id"));
        }

        let canApplyCharacterPoint = function (li) {
            let result = false;
            if (li.find(".dice-roll").length) {
                let message = game.messages.get(li.attr("data-message-id"));
                if (message.data.speaker.actor) {
                    let actor = game.actors.get(message.data.speaker.actor);
                    if ((game.user.isGM || actor.isOwner) &&
                        actor.data.type === "character" &&
                        actor.data.data.characterpoints.value > 0) {
                        result = true;
                    }
                }
            }
            return result;
        };

        options.push(
            {
                name: game.i18n.localize("OD6S.USE_A_CHARACTER_POINT"),
                icon: '<i class="fas fa-user-plus"></i>',
                condition: canApplyCharacterPoint,
                callback: li => {
                    let message = game.messages.get(li.attr("data-message-id"));
                    game.actors.get(message.data.speaker.actor).useCharacterPointOnRoll(message, message.getRollData());
                }
            }
        )
    }
}

export default class OD6SEditDifficulty extends FormApplication {
    static get defaultOptions() {
        const options = super.defaultOptions;
        options.id = "edit-difficulty";
        options.template = "systems/od6s/templates/chat/edit-difficulty.html";
        options.height = 200;
        options.width = 100;
        options.minimizable = true;
        options.title = game.i18n.localize("OD6S.EDIT_DIFFICULTY");
        return options;
    }

    getData() {
        let data = super.getData()
        return data;
    }

    async _updateObject(ev, formData) {
        let success = false;
        if (ev.submitter.value === 'cancel') {
            return;
        }
        const message = game.messages.get(formData.messageId);
        const diff = (+formData.baseDifficulty) - (+message.getFlag('od6s','baseDifficulty'));

        message.roll.total > (+message.getFlag('od6s','difficulty')) + (+diff) ? success = true : success = false;

        await message.setFlag('od6s','baseDifficulty', formData.baseDifficulty);
        await message.setFlag('od6s','difficulty', (+message.getFlag('od6s','difficulty')) + (+diff));
        await message.setFlag('od6s','success', success);
    }
}

export class OD6SEditDamage extends FormApplication {
    static get defaultOptions() {
        const options = super.defaultOptions;
        options.id = "edit-damage";
        options.template = "systems/od6s/templates/chat/edit-damage.html";
        options.height = 200;
        options.width = 100;
        options.minimizable = true;
        options.title = game.i18n.localize("OD6S.EDIT_DAMAGE");
        return options;
    }

    getData() {
        let data = super.getData()
        return data;
    }

    async _updateObject(ev, formData) {
        if (ev.submitter.value === 'cancel') {
            return;
        }
        const message = game.messages.get(formData.messageId);
        const damageScore = od6sutilities.getScoreFromDice(formData.damageDice,formData.damagePips);
        await message.setFlag('od6s','damageScore',damageScore);
    }
}

export class OD6SChooseTarget extends FormApplication {
    static get defaultOptions() {
        const options = super.defaultOptions;
        options.id = "choose-target";
        options.template = "systems/od6s/templates/chat/choose-target.html";
        options.height = 200;
        options.width = 100;
        options.minimizable = true;
        options.title = game.i18n.localize("OD6S.CHOOSE_TARGET");
        return options;
    }

    getData() {
        let data = super.getData();
        return data;
    }

    async _updateObject(ev, formData) {
        if (ev.submitter.value === 'cancel') {
            return;
        }
        const message = game.messages.get(formData.messageId);
        await message.setFlag('od6s','targetId', formData.choosetarget);
        await message.setFlag('od6s','targetName', game.scenes.active.data.tokens.find(t => t.id === formData.choosetarget).name);
    }
}

export class OD6SHandleWildDieForm extends FormApplication{
    static get defaultOptions() {
        const options = super.defaultOptions;
        options.id = "wilddie";
        options.template = "systems/od6s/templates/chat/wild-die.html";
        options.height = 200;
        options.width = 100;
        options.minimizable = true;
        options.title = game.i18n.localize("OD6S.WILD_DIE");
        return options;
    }

    getData() {
        let data = super.getData();
        return data;
    }

    async _updateObject(ev,formData) {
        if (ev.submitter.value === 'cancel') {
            return;
        }

        const message = game.messages.get(formData.messageId);
        switch(formData.wilddie) {
            case '0':
                await message.setFlag('od6s','wild', false);
                break;
            case '1':
                await message.setFlag('od6s', 'wildResult', 'OD6S.REMOVE_HIGHEST_DIE')
                // Update original card and re-display
                let replacementRoll = JSON.parse(JSON.stringify(message.roll));
                let highest = 0;
                for (let i=0; i<replacementRoll.terms[0].results.length; i++) {
                    replacementRoll.terms[0].results[i].result >
                    replacementRoll.terms[0].results[highest].result ?
                        highest = i : {}
                }
                replacementRoll.terms[0].results[highest].discarded = true;
                replacementRoll.terms[0].results[highest].active = false;
                replacementRoll.total -= (+replacementRoll.terms[0].results[highest].result);
                const messageUpdate = {};
                messageUpdate.data = {};
                messageUpdate.content = replacementRoll.total;
                messageUpdate.id = message.id;
                messageUpdate.roll = replacementRoll;

                if(message.getFlag('od6s','difficulty') && message.getFlag('od6s','success')) {
                    replacementRoll.total < message.getFlag('od6s','difficulty') ? message.setFlag('od6s','success', false) :
                        message.setFlag('od6s','success',true);
                }

                await message.update(messageUpdate, {"diff": true});

                break;
            case '2':
                await message.setFlag('od6s', 'wildResult','OD6S.COMPLICATION');
                break
        }
        await message.setFlag('od6s','wildHandled', true);
    }

}