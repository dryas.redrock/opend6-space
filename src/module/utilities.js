import OD6S from "./config/config-od6s.js";

export class od6sutilities {

    /**
     * Function which returns a number of dice and pips from a raw score.
     * e.g. a score of 14 translates to "4D+2", a score of 15 is "5D+0".
     * @param score
     * @returns {{dice: number, pips: number}}
     */
    static getDiceFromScore(score) {
        const dice = Math.floor(score / 3);
        const pips = score % 3;
        return {
            dice,
            pips
        }
    }

    /**
     * Get a score from a number of dice and pips.
     * @param dice
     * @param pips
     * @returns {number}
     */
    static getScoreFromDice(dice, pips) {
        return (+dice * 3) + (+pips);
    }

    /**
     * Calculate the strength damage score.  This is one-half the dice of either lifting or strength
     *
     * @param {Object} actorData The actor to get the score of
     *
     * @return {undefined}
     */
    static setStrengthDamageBonus(actorData) {
        if (actorData.type === 'vehicle') return;
            // See if the actor has the "lift" skill
        if (game.settings.get('od6s','strength_damage')) {
            actorData.data.strengthdamage.score = actorData.data.attributes.str.score
                + actorData.data.strengthdamage.mod;
        } else {
            const lift = actorData.items.find(skill => skill.name === "Lift");
            if (lift != null) {
                actorData.data.strengthdamage.score =
                    Math.ceil((Math.floor((lift.data.data.score + actorData.data.attributes.str.score) / 3)) / 2)
                        * 3 + actorData.data.strengthdamage.mod;
            } else {
                actorData.data.strengthdamage.score =
                    Math.ceil(Math.floor(actorData.data.attributes.str.score / 3) / 2) * 3  +
                    actorData.data.strengthdamage.mod;
            }
        }
    }

    /**
     * Get the action penalty from the actor's wound level vs. the system wound levels
     * @param actorData
     * @returns {number}
     */
    static getWoundPenalty(actor) {
        return (actor.type === 'character') ?
            OD6S.deadliness[ game.settings.get('od6s','deadliness') ][actor.data.data.wounds.value].penalty :
            OD6S.deadliness[ game.settings.get('od6s','npc-deadliness') ][actor.data.data.wounds.value].penalty;
    }

    /**
     *
     * Set initiative for an actor
     *
     * @param actorData
     *
     */
    static setInitiative(actorData) {
        if (actorData.type === 'vehicle') return;
        let formula;
        // Base init is the character's perception score.  Special abilities and optional rules may add to it.
        let score = actorData.data.attributes.per.score + actorData.data.initiative.mod;
        let dice = this.getDiceFromScore(score);
        let tiebreaker = actorData.data.attributes.per.score/100 + actorData.data.attributes.agi.score/100;
        dice.dice--;
        formula = dice.dice + "d6[Base]" + "+" + dice.pips + "+1d6x6[Wild]+" + tiebreaker;
        actorData.data.initiative.formula = formula;
        actorData.data.initiative.score = score;
    }

    /**
     * Get the initiative score of an actor
     * @param actor
     * @returns {*}
     */
    static getInitiative(actor) {
        return actor.data.data.initiative.score;
    }

    /**
     * Search for and get an item from compendia by name
     * @param itemName
     * @returns {Promise<Entity|null>}
     * @private
     */
    static async _getItemFromCompendium(itemName) {
        let itemList = '';
        let packs = '';
        game.packs.keys();
        if (game.settings.get('od6s','hide_compendia')) {
            packs = await game.packs.filter(p => p.metadata.system !== 'od6s')
        } else {
            packs = await game.packs;
        }
        for (let p of packs) {
            await p.getIndex().then(index => itemList = index);
            let searchResult = itemList.find(t => t.name === itemName);
            if (searchResult) {
                return await p.getDocument(searchResult._id);
            }
        }
        return null;
    }

    /**
     * Get an item from the world
     * @param itemName
     * @returns {Promise<*>}
     * @private
     */
    static async _getItemFromWorld(itemName) {
        return game.items.contents.find(t => t.name === itemName);
    }

    /**
     * Get all items of a certain type from compendia
     * @param itemType
     * @returns {Promise<[]>}
     */
    static async getItemsFromCompendiumByType(itemType) {
        let searchResult = [];
        let packs = '';
        game.packs.keys();
        if (game.settings.get('od6s','hide_compendia')) {
            packs = await game.packs.filter(p => p.metadata.system !== 'od6s' && p.documentName === 'Item')
        } else {
            packs = await game.packs.filter(p => p.documentName === 'Item');
        }

        for (let p of packs) {
            const items = p.index.filter(i => i.type === itemType);
            searchResult = searchResult.concat(items);
        }
        
        searchResult.sort((a, b) => a.name.localeCompare(b.name))
        return searchResult;
    }

    /**
     * Get all items of a certain type from the world
     * @param itemType
     * @returns {Promise<[]>}
     */
    static async getItemsFromWorldByType(itemType) {
        let searchResult = [];
        for (let i = 0; i < game.items.contents.length; i++){
            if (game.items.contents[i].type === itemType) {
                let item = {
                    name: game.items.contents[i].data.name,
                    type: game.items.contents[i].type,
                    description: game.items.contents[i].data.data.description
                }
                searchResult.push(item);
            }
        }
        return searchResult;
    }
}


