const OD6S = {};

OD6S.deadliness = {
    0: {
        0: {
            "description": "OD6S.WOUNDS_HEALTHY",
            "penalty": 0,
        },
        1: {
            "description": "OD6S.WOUNDS_STUNNED_1",
            "penalty": 1,
        },
        2: {
            "description": "OD6S.WOUNDS_STUNNED_2",
            "penalty": 1,
        },
        3: {
            "description": "OD6S.WOUNDS_WOUNDED_1",
            "penalty": 1,
        },
        4: {
            "description": "OD6S.WOUNDS_WOUNDED_2",
            "penalty": 1,
        },
        5: {
            "description": "OD6S.WOUNDS_SEVERELY_WOUNDED",
            "penalty": 2,
        },
        6: {
            "description": "OD6S.WOUNDS_INCAPACITATED",
            "penalty": 3,
        },
        7: {
            "description": "OD6S.WOUNDS_MORTALLY_WOUNDED",
            "penalty": 0,
        },
        8: {
            "description": "OD6S.WOUNDS_DEAD",
            "penalty": 0,
        },
    },
    2: {
        0: {
            "description": "OD6S.WOUNDS_HEALTHY",
            "penalty": 0,
        },
        1: {
            "description": "OD6S.WOUNDS_STUNNED",
            "penalty": 1,
        },
        2: {
            "description": "OD6S.WOUNDS_WOUNDED_1",
            "penalty": 1,
        },
        3: {
            "description": "OD6S.WOUNDS_WOUNDED_2",
            "penalty": 1,
        },
        4: {
            "description": "OD6S.WOUNDS_SEVERELY_WOUNDED",
            "penalty": 2,
        },
        5: {
            "description": "OD6S.WOUNDS_INCAPACITATED",
            "penalty": 3,
        },
        6: {
            "description": "OD6S.WOUNDS_MORTALLY_WOUNDED",
            "penalty": 0,
        },
        7: {
            "description": "OD6S.WOUNDS_DEAD",
            "penalty": 0,
        },
    },
    3: {
        0: {
            "description": "OD6S.WOUNDS_HEALTHY",
            "penalty": 0,
        },
        1: {
            "description": "OD6S.WOUNDS_STUNNED",
            "penalty": 1,
        },
        2: {
            "description": "OD6S.WOUNDS_WOUNDED",
            "penalty": 1,
        },
        3: {
            "description": "OD6S.WOUNDS_SEVERELY_WOUNDED",
            "penalty": 2,
        },
        4: {
            "description": "OD6S.WOUNDS_INCAPACITATED",
            "penalty": 3,
        },
        5: {
            "description": "OD6S.WOUNDS_MORTALLY_WOUNDED",
            "penalty": 0,
        },
        6: {
            "description": "OD6S.WOUNDS_DEAD",
            "penalty": 0,
        },
    },
    4: {
        0: {
            "description": "OD6S.WOUNDS_HEALTHY",
            "penalty": 0,
        },
        1: {
            "description": "OD6S.WOUNDS_WOUNDED",
            "penalty": 1,
        },
        2: {
            "description": "OD6S.WOUNDS_SEVERELY_WOUNDED",
            "penalty": 2,
        },
        3: {
            "description": "OD6S.WOUNDS_INCAPACITATED",
            "penalty": 3,
        },
        4: {
            "description": "OD6S.WOUNDS_MORTALLY_WOUNDED",
            "penalty": 0,
        },
        5: {
            "description": "OD6S.WOUNDS_DEAD",
            "penalty": 0,
        },
    },
    5: {
        0: {
            "description": "OD6S.WOUNDS_HEALTHY",
            "penalty": 0,
        },
        1: {
            "description": "OD6S.WOUNDS_WOUNDED",
            "penalty": 1,
        },
        2: {
            "description": "OD6S.WOUNDS_SEVERELY_WOUNDED",
            "penalty": 2,
        },
        3: {
            "description": "OD6S.WOUNDS_INCAPACITATED",
            "penalty": 3,
        },
        4: {
            "description": "OD6S.WOUNDS_DEAD",
            "penalty": 0,
        },
    }
}

OD6S.damage = {
    "OD6S.WOUNDS_STUNNED": 1,
    "OD6S.WOUNDS_WOUNDED": 4,
    "OD6S.WOUNDS_INCAPACITATED": 9,
    "OD6S.WOUNDS_MORTALLY_WOUNDED": 13,
    "OD6S.WOUNDS_DEAD": 16
}

OD6S.weaponTypes = [
    "OD6S.RANGED",
    "OD6S.MELEE",
    "OD6S.THROWN"
]

OD6S.actions = {
    "ranged_attack": {
        "name": "OD6S.ACTION_RANGED_ATTACK",
        "type": "rangedattack",
        "rollable": true,
        "base": "agi",
        "skill": ""
    },
    "melee_attack": {
        "name": "OD6S.ACTION_MELEE_ATTACK",
        "type": "meleeattack",
        "rollable": true,
        "base": "agi",
        "skill": "Melee Combat"
    },
    "brawl_attack": {
        "name": "OD6S.ACTION_BRAWL_ATTACK",
        "type": "brawlattack",
        "rollable": true,
        "base": "agi",
        "skill": "Brawling"
    },
    "dodge": {
        "name": "OD6S.ACTION_DODGE",
        "type": "dodge",
        "rollable": true,
        "base": "agi",
        "skill": "Dodge"
    },
    "parry": {
        "name": "OD6S.ACTION_PARRY",
        "type": "parry",
        "rollable": true,
        "base": "agi",
        "skill": "Melee Combat"
    },
    "block": {
        "name": "OD6S.ACTION_BLOCK",
        "type": "block",
        "rollable": true,
        "base": "agi",
        "skill": "Brawling"
    },
    "other": {
        "name": "OD6S.ACTION_OTHER",
        "type": "action",
        "rollable": false
    }
}

OD6S.baseHitDifficulty = 10;

OD6S.difficulty = {
    "OD6S.DIFFICULTY_UNKNOWN": {
        "min": 0,
        "max": 0
    },
    "OD6S.DIFFICULTY_CUSTOM": {
        "min": 0,
        "max": 0
    },
    "OD6S.DIFFICULTY_AUTOMATIC": {
        "min": 0,
        "max": 0
    },
    "OD6S.DIFFICULTY_VERY_EASY": {
        "min": 1,
        "max": 5
    },
    "OD6S.DIFFICULTY_EASY": {
        "min": 6,
        "max": 10
    },
    "OD6S.DIFFICULTY_MODERATE": {
        "min": 11,
        "max": 15
    },
    "OD6S.DIFFICULTY_DIFFICULT": {
        "min": 16,
        "max": 20
    },
    "OD6S.DIFFICULTY_VERY_DIFFICULT": {
        "min": 21,
        "max": 25
    },
    "OD6S.DIFFICULTY_HEROIC": {
        "min": 26,
        "max": 30
    },
    "OD6S.DIFFICULTY_LEGENDARY": {
        "min": 31,
        "max": 40
    }
}

OD6S.result = {
    "OD6S.FAILURE": {
        "description": "OD6S.FAILURE",
        "difference": -1
    },
    "OD6S.RESULT_MINIMAL": {
        "description": "OD6S.RESULT_MINIMAL_DESCRIPTION",
        "difference": 0
    },
    "OD6S.RESULT_SOLID": {
        "description": "OD6S.RESULT_SOLID_DESCRIPTION",
        "difference": 1
    },
    "OD6S.RESULT_GOOD": {
        "description": "OD6S.RESULT_GOOD_DESCRIPTION",
        "difference": 5
    },
    "OD6S.RESULT_SUPERIOR": {
        "description": "OD6S.RESULT_SUPERIOR_DESCRIPTION",
        "difference": 9
    },
    "OD6S.RESULT_SPECTACULAR": {
        "description": "OD6S.RESULT_SPECTACULAR_DESCRIPTION",
        "difference": 13
    },
    "OD6S.RESULT_INCREDIBLE": {
        "description": "OD6S.RESULT_INCREDIBLE_DESCRIPTION",
        "difference": 16
    }
}

OD6S.cover = {
    "OD6S.COVER_SMOKE": {
        "OD6S.NONE": {
            "modifier": 0
        },
        "OD6S.COVER_LIGHT_SMOKE": {
            "modifier": 3
        },
        "OD6S.COVER_THICK_SMOKE": {
            "modifier": 6
        },
        "OD6S.COVER_VERY_THICK_SMOKE": {
            "modifier": 12
        },
    },
    "OD6S.COVER_LIGHT": {
        "OD6S.COVER_LIGHT_NONE": {
            "modifier": 0
        },
        "OD6S.COVER_POOR_LIGHT": {
            "modifier": 3
        },
        "OD6S.COVER_MOONLIGHT_NIGHT": {
            "modifier": 6
        },
        "OD6S.COVER_COMPLETE_DARKNESS": {
            "modifier": 12
        },
    },
    "OD6S.COVER": {
        "OD6S.NONE": {
            "modifier": 0
        },
        "OD6S.COVER_QUARTER": {
            "modifier": 3
        },
        "OD6S.COVER_HALF": {
            "modifier": 6
        },
        "OD6S.COVER_THREE_QUARTERS": {
            "modifier": 12
        },
        "OD6S.COVER_FULL": {
            "modifier": 0
        }
    }
}


// Other modifiers besides range and cover
OD6S.calledShot = {
    "OD6S.CALLED_SHOT_NONE": {
        "modifier": 0
    },
    "OD6S.CALLED_SHOT_LARGE": {
        "modifier": 3
    },
    "OD6S.CALLED_SHOT_MEDIUM": {
        "modifier": 12
    },
    "OD6S.CALLED_SHOT_SMALL": {
        "modifier": 24
    }
}

OD6S.gravity = {
    "OD6S.GRAVITY_STANDARD": {
        "modifier": 0
    },
    "OD6S.GRAVITY_LOW": {
        "modifier": -3
    },
    "OD6S.GRAVITY_NONE": {
        "modifier": -6
    },
    "OD6S.GRAVITY_HEAVY": {
        "modifier": 9
    }
}

// Other modifiers from conditions, etc.
OD6S.misc = {
    "OD6S.MISC": {
        "modifier": 0
    }
}

// attack: subtraction or addition to hit difficulty (negative numbers are in effect a bonus)
// damage: bonus or penalty to damage
// multi: whether an attack needs a ROF selection by the character for number of shots in a round
OD6S.rangedAttackOptions = {
    "OD6S.ATTACK_STANDARD": {
        "attack": 0,
        "damage": 0,
        "multi": false
    },
    "OD6S.ATTACK_RANGED_SINGLE_FIRE_AS_MULTI": {
        "attack": -3,
        "damage": +3,
        "multi": true
    },
    "OD6S.ATTACK_RANGED_FULL_AUTO": {
        "attack": -6,
        "damage": 6,
        "multi": false
    },
    "OD6S.ATTACK_RANGED_SWEEP": {
        "attack": -6,
        "damage": -9,
        "multu": false
    },
    "OD6S.ATTACK_RANGED_BURST_FIRE_AS_SINGLE": {
        "attack": 0,
        "damage": -6,
        "multi": false
    }
}

OD6S.meleeAttackOptions = {
    "OD6S.ATTACK_STANDARD": {
        "attack": 0,
        "damage": 0,
        "multi": false
    },
    "OD6S.ATTACK_ALL_OUT": {
        "attack": -6,
        "damage": 3
    },
    "OD6S.ATTACK_LUNGE": {
        "attack": 3,
        "damage": -3
    },
    "OD6S.ATTACK_KNOCKDOWN_TRIP": {
        "attack": 6,
        "damage": 0
    },
    "OD6S.ATTACK_PUSH": {
        "attack": 3,
        "damage": 0
    }
}

OD6S.brawlAttackOptions = {
    "OD6S.ATTACK_STANDARD": {
        "attack": 0,
        "damage": 0,
        "multi": false
    },
    "OD6S.ATTACK_ALL_OUT": {
        "attack": -6,
        "damage": 3
    },
    "OD6S.ATTACK_GRAB": {
        "attack": 9,
        "damage": 0
    },
    "OD6S.ATTACK_LUNGE": {
        "attack": 3,
        "damage": -3
    },
    "OD6S.ATTACK_KNOCKDOWN_TRIP": {
        "attack": 6,
        "damage": 0
    },
    "OD6S.ATTACK_PUSH": {
        "attack": 3,
        "damage": 0
    },
    "OD6S.ATTACK_SWEEP": {
        "attack": -6,
        "damage": -9
    },
    "OD6S.ATTACK_TACKLE": {
        "attack": 3,
        "damage": 0
    }
}

OD6S.attributes = {
    "agi": {
        "name": '',
        "shortName": ''
    },
    "str": {
        "name": '',
        "shortName": ''
    },
    "mec": {
        "name": '',
        "shortName": ''
    },
    "kno": {
        "name": '',
        "shortName": ''
    },
    "per": {
        "name": '',
        "shortName": ''
    },
    "tec": {
        "name": '',
        "shortName": ''
    },
    "met": {
        "name": '',
        "shortName": ''
    }
}

OD6S.ranges = {
    "OD6S.RANGE_POINT_BLANK_SHORT": {
        "name": "OD6S.RANGE_POINT_BLANK",
        "difficulty": -5
    },
    "OD6S.RANGE_SHORT_SHORT": {
        "name": "OD6S.RANGE_SHORT",
        "difficulty": 0
    },
    "OD6S.RANGE_MEDIUM_SHORT": {
        "name": "OD6S.RANGE_MEDIUM",
        "difficulty": 5
    },
    "OD6S.RANGE_LONG_SHORT": {
        "name": "OD6S.RANGE_LONG",
        "difficulty": 10
    }
}

OD6S.damageTypes = {
    "p": "OD6S.PHYSICAL",
    "e": "OD6S.ENERGY"
}

OD6S.cyberneticsLocations = [
    "OD6S.HEAD",
    "OD6S.RIGHT_ARM",
    "OD6S.LEFT_ARM",
    "OD6S.BODY",
    "OD6S.RIGHT_LEG",
    "OD6S.LEFT_LEG"
]

OD6S.itemLabels = {
    "skill": "OD6S.SKILL",
    "specialization": "OD6S.SPECIALIZATION",
    "advantage": "OD6S.ADVANTAGE",
    "disadvantage": "OD6S.DISADVANTAGE",
    "specialability": "OD6S.SPECIAL_ABILITY",
    "armor": "OD6S.ARMOR",
    "weapon": "OD6S.WEAPON",
    "gear": "OD6S.GEAR",
    "cybernetic": "OD6S.CYBERNETIC",
    "vehicle": "OD6S.VEHICLE",
    "manifestation": "OD6S.MANIFESTATION",
    "character-template": "OD6S.CHARACTER_TEMPLATE",
    "action": "OD6S.ACTION",
}

OD6S.startCombat = false;

OD6S.fatePointsName = '';
OD6S.fatePointsShortName = '';
OD6S.useAFatePointName = '';
OD6S.metaphysicsName = '';
OD6S.manifestationsName = '';
OD6S.metaphysicsExtranormalName = '';
OD6S.brawlAttribute = '';
OD6S.opposed = [];

OD6S.chatPath = 'systems/od6s/templates/chat/';
OD6S.chatTemplates = {
    "generic": OD6S.chatPath + "generic.html",
    "roll": OD6S.chatPath + "roll.html",
    "opposed": OD6S.chatPath + "opposed.html",
    "damageresult": OD6S.chatPath + "damageresult.html"
}

OD6S.data_tab = {
    "defense": {
        "dodge": "OD6S.DODGE",
        "parry": "OD6S.PARRY",
        "block": "OD6S.BLOCK"
    },
    "offense": {
        "ranged": "OD6S.RANGED",
        "melee": "OD6S.MELEE",
        "brawl": "OD6S.BRAWL"
    }
}

OD6S.statusEffects = [
    {
        id: "dead",
        label: "EFFECT.StatusDead",
        icon: "icons/svg/skull.svg"
    },
    {
        id: "unconscious",
        label: "EFFECT.StatusUnconscious",
        icon: "icons/svg/unconscious.svg"
    },
    {
        id: "sleep",
        label: "EFFECT.StatusAsleep",
        icon: "icons/svg/sleep.svg"
    },
    {
        id: "stun",
        label: "EFFECT.StatusStunned",
        icon: "icons/svg/daze.svg"
    },
    {
        id: "prone",
        label: "EFFECT.StatusProne",
        icon: "icons/svg/falling.svg"
    },
    {
        id: "restrain",
        label: "EFFECT.StatusRestrained",
        icon: "icons/svg/net.svg",
    },
    {
        id: "paralysis",
        label: "EFFECT.StatusParalysis",
        icon: "icons/svg/paralysis.svg",
    },
    {
        id: "fly",
        label: "EFFECT.StatusFlying",
        icon: "icons/svg/wing.svg",
    },
    {
        id: "blind",
        label: "EFFECT.StatusBlind",
        icon: "icons/svg/blind.svg"
    },
    {
        id: "deaf",
        label: "EFFECT.StatusDeaf",
        icon: "icons/svg/deaf.svg"
    },
    {
        id: "silence",
        label: "EFFECT.StatusSilenced",
        icon: "icons/svg/silenced.svg"
    },
    {
        id: "fear",
        label: "EFFECT.StatusFear",
        icon: "icons/svg/terror.svg"
    },
    {
        id: "burning",
        label: "EFFECT.StatusBurning",
        icon: "icons/svg/fire.svg"
    },
    {
        id: "frozen",
        label: "EFFECT.StatusFrozen",
        icon: "icons/svg/frozen.svg"
    },
    {
        id: "shock",
        label: "EFFECT.StatusShocked",
        icon: "icons/svg/lightning.svg"
    },
    {
        id: "bleeding",
        label: "EFFECT.StatusBleeding",
        icon: "icons/svg/blood.svg"
    },
    {
        id: "disease",
        label: "EFFECT.StatusDisease",
        icon: "icons/svg/biohazard.svg"
    },
    {
        id: "poison",
        label: "EFFECT.StatusPoison",
        icon: "icons/svg/poison.svg"
    },
    {
        id: "radiation",
        label: "EFFECT.StatusRadiation",
        icon: "icons/svg/radiation.svg"
    },
    {
        id: "upgrade",
        label: "EFFECT.StatusUpgrade",
        icon: "icons/svg/upgrade.svg"
    },
    {
        id: "downgrade",
        label: "EFFECT.StatusDowngrade",
        icon: "icons/svg/downgrade.svg"
    },
    {
        id: "target",
        label: "EFFECT.StatusTarget",
        icon: "icons/svg/target.svg"
    }
]

export default OD6S;