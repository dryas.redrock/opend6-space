// Import Modules
import {OD6SActor} from "./actor/actor.js";
import {OD6SActorSheet} from "./actor/actor-sheet.js";
import {OD6SItem} from "./item/item.js";
import {OD6SItemSheet} from "./item/item-sheet.js";
import {od6sutilities} from "./utilities.js";
import {OD6SCombatTracker} from "./overrides/combat-tracker.js";
import {OD6SCompendiumDirectory} from "./overrides/compendium-directory.js";
import {OD6SChatLog} from "./overrides/chat-log.js";
import OD6SEditDifficulty, {OD6SEditDamage, OD6SChooseTarget, OD6SChat, OD6SHandleWildDieForm} from "./chat.js";
import OD6SSocketHandler from "./socket.js";
//import OD6SChooseTarget from "./chat.js";
import OD6S from "./config/config-od6s.js";

Hooks.once('init', async function () {

    game.od6s = {
        OD6SActor,
        OD6SItem,
        rollItemMacro,
        simpleRoll,
        diceTerms: [CharacterPointDie, WildDie],
        config: OD6S
    };

    //CONFIG.debug.hooks = true

    game.socket.on('system.od6s', (data) => {
        if (data.operation === 'updateRollMessage') OD6SSocketHandler.updateRollMessage(data);
        if (data.operation === 'updateInitRoll') OD6SSocketHandler.updateInitRoll(data);
    })

    /**
     * Set an initiative formula for the system
     * @type {String}
     */
    CONFIG.Combat.initiative = {
        formula: "@initiative.formula",
        decimals: 2
    };

    CONFIG.ui.chat = OD6SChatLog;
    CONFIG.ui.combat = OD6SCombatTracker;
    CONFIG.ui.compendium = OD6SCompendiumDirectory;
    CONFIG.statusEffects = OD6S.statusEffects;
    CONFIG.Dice.terms["w"] = WildDie;
    CONFIG.Dice.terms["b"] = CharacterPointDie;

    CONFIG.ChatMessage.template = "systems/od6s/templates/chat/chat.html";

    // System Settings
    game.settings.register("od6s", "hide_compendia", {
        name: game.i18n.localize("OD6S.CONFIG_HIDE_COMPENDIA"),
        hint: game.i18n.localize("OD6S.CONFIG_HIDE_COMPENDIA_DESCRIPTION"),
        scope: "world",
        config: true,
        default: false,
        type: Boolean,
        onChange: () => {
            game.compendiumDirectory.render();
        }
    })

    game.settings.register("od6s", "hide_advantages_disadvantages", {
        name: game.i18n.localize("OD6S.CONFIG_HIDE_ADVANTAGES_DISADVANTAGES"),
        hint: game.i18n.localize("OD6S.CONFIG_HIDE_ADVANTAGES_DISADVANTAGES_DESCRIPTION"),
        scope: "world",
        config: true,
        default: false,
        type: Boolean,
        onChange: () => {
            game.compendiumDirectory.render();
        }
    })

    game.settings.register("od6s", "deadliness", {
        name: game.i18n.localize("OD6S.CONFIG_DEADLINESS"),
        hint: game.i18n.localize("OD6S.CONFIG_DEADLINESS_DESCRIPTION"),
        scope: "world",
        config: true,
        default: 3,
        type: Number,
        choices: {
            "1": game.i18n.localize("OD6S.CONFIG_DEADLINESS_1"),
            "2": game.i18n.localize("OD6S.CONFIG_DEADLINESS_2"),
            "3": game.i18n.localize("OD6S.CONFIG_DEADLINESS_3"),
            "4": game.i18n.localize("OD6S.CONFIG_DEADLINESS_4"),
            "5": game.i18n.localize("OD6S.CONFIG_DEADLINESS_5")
        }
    })

    game.settings.register("od6s", "npc-deadliness", {
        name: game.i18n.localize("OD6S.CONFIG_NPC_DEADLINESS"),
        hint: game.i18n.localize("OD6S.CONFIG_NPC_DEADLINESS_DESCRIPTION"),
        scope: "world",
        config: true,
        default: 4,
        type: Number,
        choices: {
            "1": game.i18n.localize("OD6S.CONFIG_DEADLINESS_1"),
            "2": game.i18n.localize("OD6S.CONFIG_DEADLINESS_2"),
            "3": game.i18n.localize("OD6S.CONFIG_DEADLINESS_3"),
            "4": game.i18n.localize("OD6S.CONFIG_DEADLINESS_4"),
            "5": game.i18n.localize("OD6S.CONFIG_DEADLINESS_5")
        }
    })

    game.settings.register("od6s", "hide-skill-cards", {
        name: game.i18n.localize("OD6S.CONFIG_HIDE_SKILL_ROLLS"),
        hint: game.i18n.localize("OD6S.CONFIG_HIDE_SKILL_ROLLS_DESCRIPTION"),
        scope: "world",
        config: "true",
        type: Boolean,
        default: true
    })

    game.settings.register("od6s", "hide-combat-cards", {
        name: game.i18n.localize("OD6S.CONFIG_HIDE_ATTACK_ROLLS"),
        hint: game.i18n.localize("OD6S.CONFIG_HIDE_ATTACK_ROLLS_DESCRIPTION"),
        scope: "world",
        config: "true",
        type: Boolean,
        default: true
    })

    game.settings.register("od6s", "roll-modifiers", {
        name: game.i18n.localize("OD6S.CONFIG_SHOW_MODIFIERS"),
        hint: game.i18n.localize("OD6S.CONFIG_SHOW_MODIFIERS_DESCRIPTION"),
        scope: "world",
        config: "true",
        type: Boolean,
        default: true
    })

    game.settings.register("od6s", "hide-gm-rolls", {
        name: game.i18n.localize("OD6S.CONFIG_HIDE_GM_ROLLS"),
        hint: game.i18n.localize("OD6S.CONFIG_HIDE_GM_ROLLS_DESCRIPTION"),
        scope: "world",
        config: "true",
        type: Boolean,
        default: true
    })

    game.settings.register("od6s", "use_wild_die", {
        name: game.i18n.localize("OD6S.CONFIG_USE_WILD_DIE"),
        hint: game.i18n.localize("OD6S.CONFIG_USE_WILD_DIE_DESCRIPTION"),
        scope: "world",
        config: "true",
        type: Boolean,
        default: true
    })

    /* TODO
    game.settings.register("od6s", "bodypoints", {
      name:  game.i18n.localize("OD6S.CONFIG_USE_BODY"),
      hint:  game.i18n.localize("OD6S.CONFIG_USE_BODY_DESCRIPTION"),
      scope: "world",
      config: true,
      default: 0,
      type: Number,
      choices: {
        "0":  game.i18n.localize("OD6S.CONFIG_USE_WOUNDS"),
        "1":  game.i18n.localize("OD6S.CONFIG_USE_WOUNDS_WITH_BODY"),
        "2":  game.i18n.localize("OD6S.CONFIG_USE_BODY_ONLY")
      }
    })

    game.settings.register("od6s", "highhitdamage", {
      name:  game.i18n.localize("OD6S.CONFIG_USE_OPTIONAL_DAMAGE"),
      hint:  game.i18n.localize("OD6S.CONFIG_USE_OPTIONAL_DAMAGE_DESCRIPTION"),
      scope: "world",
      config: true,
      default: false,
      type: Boolean
    })

    game.settings.register("od6s", "initoptions", {
      name:  game.i18n.localize("OD6S.CONFIG_INITIATIVE_SETTING"),
      hint:  game.i18n.localize("OD6S.CONFIG_INITIATIVE_SETTING_DESCRIPTION"),
      scope: "world",
      config: true,
      default: 1,
      type: Number,
      choices: {
        "0":  game.i18n.localize("OD6S.CONFIG_INITIATIVE_1"),
        "1":  game.i18n.localize("OD6S.CONFIG_INITIATIVE_2"),
        "2":  game.i18n.localize("OD6S.CONFIG_INITIATIVE_3"),
        "3":  game.i18n.localize("OD6S.CONFIG_INITIATIVE_4")
      }
    })

    game.settings.register("od6s", "initbonus", {
      name:  game.i18n.localize("OD6S.CONFIG_USE_INIT_BONUS"),
      hint:  game.i18n.localize("OD6S.CONFIG_USE_INIT_BONUS_DESCRIPTION"),
      scope: "world",
      config: true,
      default: false,
      type: Boolean
    })

    game.settings.register("od6s", "brawlstrength", {
      name:  game.i18n.localize("OD6S.CONFIG_USE_STRENGTH_BRAWL"),
      hint:  game.i18n.localize("OD6S.CONFIG_USE_STRENGTH_BRAWL_DESCRIPTION"),
      scope: "world",
      config: true,
      default: false,
      type: Boolean
    })

    game.settings.register("od6s", "fastcombat", {
      name:  game.i18n.localize("OD6S.CONFIG_FAST_COMBAT"),
      hint:  game.i18n.localize("OD6S.CONFIG_FAST_COMBAT_DESCRIPTION"),
      scope: "world",
      config: true,
      default: false,
      type: Boolean
    })
    */

    game.settings.register("od6s", "customize_fate_points", {
        name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_FATE_POINTS"),
        hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_FATE_POINTS_DESCRIPTION"),
        scope: "world",
        config: true,
        type: String,
        default: '',
        onChange: value => (value ? OD6S.fatePointsName = value : game.i18n.localize('OD6S.CHAR_FATE_POINTS'))
    })

    game.settings.register("od6s", "customize_fate_points_short", {
        name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_FATE_POINTS_SHORT"),
        hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_FATE_POINTS_SHORT_DESCRIPTION"),
        scope: "world",
        config: true,
        type: String,
        default: "",
        onChange: value => (value ? OD6S.fatePointsShortName = value : game.i18n.localize('OD6S.CHAR_FATE_POINTS_SHORT'))
    })

    game.settings.register("od6s", "customize_use_a_fate_point", {
        name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_USE_FATE_POINT"),
        hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_USE_FATE_POINT_DESCRIPTION"),
        scope: "world",
        config: true,
        type: String,
        default: "",
        onChange: value => (value ? OD6S.useAFatePointName = value : game.i18n.localize('OD6S.CHAR_USE_FATE_POINT'))
    })

    game.settings.register("od6s", "custom_field_1", {
        name: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_1"),
        hint: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_1_DESCRIPTION"),
        scope: "world",
        config: true,
        default: "",
        type: String
    })

    game.settings.register("od6s", "custom_field_1_short", {
        name: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_1_SHORT"),
        hint: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_1_SHORT_DESCRIPTION"),
        scope: "world",
        config: true,
        default: "",
        type: String
    })

    game.settings.register("od6s", "custom_field_1_type", {
        name: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_1_TYPE"),
        hint: game.i18n.localize("OD6S.CONFIG_CUSTOM_CHARACTER_SHEET_FIELD_1_TYPE_DESCRIPTION"),
        scope: "world",
        config: true,
        default: "",
        type: String,
        choices: {
            "number": game.i18n.localize("OD6S.NUMBER"),
            "string": game.i18n.localize("OD6S.STRING")
        }
    })

    game.settings.register("od6s", "customize_agility_name", {
        name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_AGILITY_NAME"),
        hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_AGILITY_NAME_DESCRIPTION"),
        scope: "world",
        config: true,
        type: String,
        default: '',
        onChange: value => (value ? OD6S.attributes.agi.name = value : game.i18n.localize('OD6S.CHAR_AGILITY'))
    })

    game.settings.register("od6s", "customize_agility_name_short", {
        name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_AGILITY_NAME_SHORT"),
        hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_AGILITY_NAME_SHORT_DESCRIPTION"),
        scope: "world",
        config: true,
        type: String,
        default: "",
        onChange: value => (value ? OD6S.attributes.agi.shortName = value : game.i18n.localize('OD6S.CHAR_AGILITY_SHORT'))
    })

    game.settings.register("od6s", "customize_strength_name", {
        name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_STRENGTH_NAME"),
        hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_STRENGTH_NAME_DESCRIPTION"),
        scope: "world",
        config: true,
        type: String,
        default: '',
        onChange: value => (value ? OD6S.attributes.str.name = value : game.i18n.localize('OD6S.CHAR_STRENGTH'))
    })

    game.settings.register("od6s", "customize_strength_name_short", {
        name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_STRENGTH_NAME_SHORT"),
        hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_STRENGTH_NAME_SHORT_DESCRIPTION"),
        scope: "world",
        config: true,
        type: String,
        default: "",
        onChange: value => (value ? OD6S.attributes.str.shortName = value : game.i18n.localize('OD6S.CHAR_STRENGTH_SHORT'))
    })

    game.settings.register("od6s", "customize_mechanical_name", {
        name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_MECHANICAL_NAME"),
        hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_MECHANICAL_NAME_DESCRIPTION"),
        scope: "world",
        config: true,
        type: String,
        default: '',
        onChange: value => (value ? OD6S.attributes.mec.name = value : game.i18n.localize('OD6S.CHAR_MECHANICAL'))
    })

    game.settings.register("od6s", "customize_mechanical_name_short", {
        name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_MECHANICAL_NAME_SHORT"),
        hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_MECHANICAL_NAME_SHORT_DESCRIPTION"),
        scope: "world",
        config: true,
        type: String,
        default: "",
        onChange: value => (value ? OD6S.attributes.mec.shortName = value : game.i18n.localize('OD6S.CHAR_MECHANICAL_SHORT'))
    })

    game.settings.register("od6s", "customize_knowledge_name", {
        name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_KNOWLEDGE_NAME"),
        hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_KNOWLEDGE_NAME_DESCRIPTION"),
        scope: "world",
        config: true,
        type: String,
        default: '',
        onChange: value => (value ? OD6S.attributes.kno.name = value : game.i18n.localize('OD6S.CHAR_KNOWLEDGE'))
    })

    game.settings.register("od6s", "customize_knowledge_name_short", {
        name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_KNOWLEDGE_NAME_SHORT"),
        hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_KNOWLEDGE_NAME_SHORT_DESCRIPTION"),
        scope: "world",
        config: true,
        type: String,
        default: "",
        onChange: value => (value ? OD6S.attributes.kno.shortName = value : game.i18n.localize('OD6S.CHAR_KNOWLEDGE_SHORT'))
    })

    game.settings.register("od6s", "customize_perception_name", {
        name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_PERCEPTION_NAME"),
        hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_PERCEPTION_NAME_DESCRIPTION"),
        scope: "world",
        config: true,
        type: String,
        default: '',
        onChange: value => (value ? OD6S.attributes.per.name = value : game.i18n.localize('OD6S.CHAR_PERCEPTION'))
    })

    game.settings.register("od6s", "customize_perception_name_short", {
        name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_PERCEPTION_NAME_SHORT"),
        hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_PERCEPTION_NAME_SHORT_DESCRIPTION"),
        scope: "world",
        config: true,
        type: String,
        default: "",
        onChange: value => (value ? OD6S.attributes.per.shortName = value : game.i18n.localize('OD6S.CHAR_PERCEPTION_NAME'))
    })

    game.settings.register("od6s", "customize_technical_name", {
        name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_TECHNICAL_NAME"),
        hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_TECHNICAL_NAME_DESCRIPTION"),
        scope: "world",
        config: true,
        type: String,
        default: '',
        onChange: value => (value ? OD6S.attributes.tec.name = value : game.i18n.localize('OD6S.CHAR_TECHNICAL'))
    })

    game.settings.register("od6s", "customize_technical_name_short", {
        name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_TECHNICAL_NAME_SHORT"),
        hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_TECHNICAL_NAME_SHORT_DESCRIPTION"),
        scope: "world",
        config: true,
        type: String,
        default: "",
        onChange: value => (value ? OD6S.attributes.tec.shortName = value : game.i18n.localize('OD6S.CHAR_TECHNICAL_NAME'))
    })

    game.settings.register("od6s", "customize_metaphysics_name", {
        name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_METAPHYSICS_NAME"),
        hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_METAPHYSICS_NAME_DESCRIPTION"),
        scope: "world",
        config: true,
        type: String,
        default: "",
        onChange: value => (value ? OD6S.attributes.met.name = value : game.i18n.localize('OD6S.CHAR_METAPHYSICS'))
    })

    game.settings.register("od6s", "customize_metaphysics_name_short", {
        name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_METAPHYSICS_NAME_SHORT"),
        hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_METAPHYSICS_NAME_SHORT_DESCRIPTION"),
        scope: "world",
        config: true,
        type: String,
        default: "",
        onChange: value => (value ? OD6S.attributes.met.name = value : game.i18n.localize('OD6S.CHAR_METAPHYSICS'))
    })

    game.settings.register("od6s", "customize_manifestations", {
        name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_MANIFESTATIONS"),
        hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_MANIFESTATIONS_DESCRIPTION"),
        scope: "world",
        config: true,
        default: "",
        type: String,
        onChange: value => (value ? OD6S.manifestationsName = value : game.i18n.localize('OD6S.CHAR_MANIFESTATIONS'))
    })

    game.settings.register("od6s", "customize_manifestation", {
        name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_MANIFESTATION"),
        hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_MANIFESTATION_DESCRIPTION"),
        scope: "world",
        config: true,
        default: "",
        type: String,
        onChange: value => (value ? OD6S.manifestationName = value : game.i18n.localize('OD6S.CHAR_MANIFESTATIONS'))
    })

    game.settings.register("od6s", "metaphysics_attribute_optional", {
        name: game.i18n.localize("OD6S.CONFIG_METAPHYSICS_ATTRIBUTE_OPTIONAL"),
        hint: game.i18n.localize("OD6S.CONFIG_METAPHYSICS_ATTRIBUTE_OPTIONAL_DESCRIPTION"),
        scope: "world",
        config: true,
        type: Boolean,
        default: false
    })

    game.settings.register("od6s", "customize_metaphysics_extranormal", {
        name: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_METAPHYSICS_EXTRANORMAL"),
        hint: game.i18n.localize("OD6S.CONFIG_CUSTOMIZE_METAPHYSICS_EXTRANORMAL_DESCRIPTION"),
        scope: "world",
        config: true,
        default: "",
        type: String,
        onChange: value => (value ? OD6S.metaphysicsExtranormalName = value : game.i18n.localize('OD6S.CHAR_METAPHYSICS_EXTRANORMAL'))
    })

    game.settings.register("od6s", "brawl_attribute", {
        name: game.i18n.localize("OD6S.CONFIG_BRAWL_ATTRIBUTE"),
        hint: game.i18n.localize("OD6S.CONFIG_BRAWL_ATTRIBUTE_DESCRIPTION"),
        scope: "world",
        config: true,
        default: "agi",
        type: String,
        choices: {
            "agi": game.i18n.localize("OD6S.CHAR_AGILITY"),
            "str": game.i18n.localize("OD6S.CHAR_STRENGTH"),
        }
    })

    game.settings.register("od6s", "parry_skills", {
        name: game.i18n.localize("OD6S.CONFIG_PARRY_SKILLS"),
        hint: game.i18n.localize("OD6S.CONFIG_PARRY_SKILLS_DESCRIPTION"),
        scope: "world",
        config: true,
        type: Boolean,
        default: false
    })

    game.settings.register("od6s", "reaction_skills", {
        name: game.i18n.localize("OD6S.CONFIG_REACTION_SKILLS"),
        hint: game.i18n.localize("OD6S.CONFIG_REACTION_SKILLS_DESCRIPTION"),
        scope: "world",
        config: true,
        type: Boolean,
        default: false
    })

    game.settings.register("od6s","strength_damage", {
        name: game.i18n.localize("OD6S.CONFIG_STRENGTH_DAMAGE"),
        hint: game.i18n.localize("OD6S.CONFIG_STRENGTH_DAMAGE_DESCRIPTION"),
        scope: "world",
        config: true,
        type: Boolean,
        default: false
    })
    game.settings.register("od6s", "wild_die_one_face", {
        name: game.i18n.localize('OD6S.CONFIG_WILD_DIE_ONE_FACE'),
        hint: game.i18n.localize("OD6S.CONFIG_WILD_DIE_ONE_FACE_DESCRIPTION"),
        scope: "world",
        config: true,
        default: "systems/od6s/icons/skull-shield.png",
        type: String,
        filePicker: "image"
    })

    game.settings.register("od6s", "wild_die_six_face", {
        name: game.i18n.localize('OD6S.CONFIG_WILD_DIE_SIX_FACE'),
        hint: game.i18n.localize("OD6S.CONFIG_WILD_DIE_SIX_FACE_DESCRIPTION"),
        scope: "world",
        config: true,
        default: "systems/od6s/icons/eclipse-flare.png",
        type: String,
        filePicker: "image"
    })

    // Define custom Entity classes
    CONFIG.Actor.documentClass = OD6SActor;
    CONFIG.Item.documentClass = OD6SItem;

    // Register sheet application classes
    Actors.unregisterSheet("core", ActorSheet);
    Actors.registerSheet("od6s", OD6SActorSheet, {makeDefault: true});
    Items.unregisterSheet("core", ItemSheet);
    Items.registerSheet("od6s", OD6SItemSheet, {makeDefault: true});

    // If you need to add Handlebars helpers, here are a few useful examples:
    Handlebars.registerHelper('concat', function () {
        let outStr = '';
        for (let arg in arguments) {
            if (typeof arguments[arg] != 'object') {
                outStr += arguments[arg];
            }
        }
        return outStr;
    });

    Handlebars.registerHelper('getRollTypeForCard', function (type, subtype) {
        let label = '';
        switch (type) {
            case "weapon":
                switch (subtype) {
                    case "rangedattack":
                        label = "OD6S.CARD_RANGED_ATTACK";
                        break;
                    case "meleeattack":
                        label = "OD6S.CARD_MELEE_ATTACK";
                        break;
                    case "brawlattack":
                        label = "OD6S.CARD_BRAWL_ATTACK";
                        break
                    default:
                }
                break;
            case "action":
                switch (subtype) {
                    case "rangedattack":
                        label = "OD6S.CARD_RANGED_ATTACK";
                        break;
                    case "meleeattack":
                        label = "OD6S.CARD_MELEE_ATTACK";
                        break;
                    case "brawlattack":
                        label = "OD6S.CARD_BRAWL_ATTACK";
                        break;
                    case "dodge":
                        label = "OD6S.CARD_DODGE";
                        break;
                    case "parry":
                        label = "OD6S.CARD_PARRY";
                        break;
                    case "block":
                        label = "OD6S.CARD_BLOCK";
                        break;
                    default:
                }
                break;

            default:
        }
        return label;
    })

    Handlebars.registerHelper('isMetaphysicsAttributeOptional', function () {
        return game.settings.get('od6s', 'metaphysics_attribute_optional');
    })

    Handlebars.registerHelper('isSkillOrAttribute', function (type, subtype) {
        if (type === "skill" || subtype === "skill" ||
            type === "specialization" || subtype === "specialization" ||
            type === "attribute" || subtype === "attribute") {
            return true;
        } else {
            return false;
        }
    })

    Handlebars.registerHelper('hitsOrMisses', function (success) {
        return success ? game.i18n.localize('OD6S.HITS') : game.i18n.localize('OD6S.MISSES');
    })

    Handlebars.registerHelper('onSuccess', function (success, roll, target) {
        //Get the level of success and return the message
        let resultMessage = '';
        if (success) {
            const difference = roll - target;
            if (difference < 0) {
                // Actually a failure
                return 'OD6S.FAILURE';
            }
            for (let result in OD6S.result) {
                if (difference >= OD6S.result[result].difference) {
                    resultMessage = result;
                } else {
                    break;
                }
            }
        } else {
            resultMessage = 'OD6S.FAILURE'
        }

        return resultMessage;
    })

    Handlebars.registerHelper('getResultDescription', function (success) {
        return OD6S.result[success].description;
    })

    Handlebars.registerHelper('isAttack', function (subtype) {
        if (typeof (subtype) === 'undefined') {
            return false;
        } else {
            return subtype.endsWith('attack');
        }
    })

    Handlebars.registerHelper('toLowerCase', function (str) {
        return str.toLowerCase();
    });

    Handlebars.registerHelper('toUpperCase', function (str) {
        return str.toUpperCase();
    });

    Handlebars.registerHelper('diceFromScore', function (score) {
        return Math.floor(score / 3);
    });

    Handlebars.registerHelper('pipsFromScore', function (score) {
        return score % 3;
    });

    Handlebars.registerHelper('and', function () {
        // Get function args and remove last one (meta object); every(Boolean) checks AND
        return Array.prototype.slice.call(arguments, 0, arguments.length - 1).every(Boolean);
    });

    Handlebars.registerHelper('getAttackOptions', function (type) {
        if (type === 'rangedattack') {
            return OD6S.rangedAttackOptions;
        }

        if (type === 'meleeattack') {
            return OD6S.meleeAttackOptions;
        }

        if (type === 'brawlattack') {
            return OD6S.brawlAttackOptions;
        }
    })

    Handlebars.registerHelper('itemTypes', function () {
        let itemTypes = {};
        const templateItems = game.data.system.template.Item;
        for (const [key, value] of Object.entries(templateItems)) {
            if (key === 'action' ||
                key === 'specialization' ||
                key === 'character-template' || key === 'templates' ||
                key === 'types') {
            } else if ((key === 'advantage' || key === 'disadvantage') &&
                game.settings.get('od6s', 'hide_advantages_disadvantages')) {
            } else {
                itemTypes[key] = value;
                if (key === "manifestation") {
                    itemTypes[key].label = OD6S.manifestationsName;
                }
            }
        }
        return itemTypes;
    })

    Handlebars.registerHelper('hideAdvantagesDisadvantages', function () {
        return game.settings.get('od6s', 'hide_advantages_disadvantages');
    })

    Handlebars.registerHelper('getWoundLevels', function (type) {
        if (type !== 'character') {
            return OD6S.deadliness[game.settings.get("od6s", "npc-deadliness")];
        } else {
            return OD6S.deadliness[game.settings.get("od6s", "deadliness")]
        }
    });

    Handlebars.registerHelper('woundsFromValue', function (value, type) {
        if (type === 'character') {
            const max = Object.keys(OD6S.deadliness[game.settings.get("od6s", "deadliness")]).length;
            if (value > max) {
                return max;
            } else {
                return OD6S.deadliness[game.settings.get("od6s", "deadliness")][value].description;
            }
        } else {
            const max = Object.keys(OD6S.deadliness[game.settings.get("od6s", "npc-deadliness")]).length;
            if (value > max) {
                return max;
            } else {
                return OD6S.deadliness[game.settings.get("od6s", "npc-deadliness")][value].description;
            }
        }
    });

    Handlebars.registerHelper('getDamageTypes', function () {
        return OD6S.damageTypes;
    })

    Handlebars.registerHelper('getWeaponTypes', function () {
        return OD6S.weaponTypes;
    })

    Handlebars.registerHelper('getMeleeDamage', function (str, weapon) {
        return (+str) + (weapon);
    })

    Handlebars.registerHelper('getActionPenalties', function (actions) {
        // Get penalties associated with the number of actions
        return (actions > 0) ? actions - 1 : 0;
    })

    Handlebars.registerHelper('getWoundPenalties', function (actor) {
        // Get penalties associated with the number of actions
        return od6sutilities.getWoundPenalty(actor)
    })

    Handlebars.registerHelper('getFlag', function (id, flag) {
        return od6sutilities.getFlag(id, flag);
    })

    Handlebars.registerHelper('getDRScore', function (actor) {
        // Get the actor's total DR
        return od6sutilities.getDamageResistance(actor)
    })

    Handlebars.registerHelper('getActions', function () {
        // Return a list of available actions
        return OD6S.actions;
    })

    Handlebars.registerHelper('getInitiative', function (actor) {
        return od6sutilities.getInitiative(actor);
    })

    Handlebars.registerHelper('isEvenAttribute', function (actor, attribute) {
        let result = 0;
        let it = 0;
        for (const key in actor.data.data.attributes) {
            if (key === attribute) {
                result = it % 2;
                break;
            }
            it++;
        }

        return result;
    })

    Handlebars.registerHelper('getMetaphysicsName', function () {
        return getAttributeName('met');
    })

    Handlebars.registerHelper('getManifestationsName', function () {
        return OD6S.manifestationsName;
    })

    Handlebars.registerHelper('getFatePointsName', function () {
        return OD6S.fatePointsName;
    })

    Handlebars.registerHelper('getFatePointsShortName', function () {
        return OD6S.fatePointsShortName;
    })

    Handlebars.registerHelper('getMetaphysicsExtranormalName', function () {
        return OD6S.metaphysicsExtranormalName;
    })

    Handlebars.registerHelper('getCustomField1', function () {
        const customField = game.settings.get('od6s', 'custom_field_1');
        if (typeof (customField) === 'undefined') {
            return "";
        }
        return customField;
    })

    Handlebars.registerHelper('getCustomField1Short', function () {
        const customField = game.settings.get('od6s', 'custom_field_1_short');
        if (typeof (customField) === 'undefined' || customField === '') {
            return game.settings.get('od6s', 'custom_field_1');
        } else {
            return game.settings.get('od6s', 'custom_field_1_short');
        }
    })

    Handlebars.registerHelper('getCustomField1Type', function () {
        const thisType = game.settings.get('od6s', 'custom_field_1_type');
        if (thisType === "string") {
            return "text";
        }
        if (thisType === "number") {
            return "number";
        }
    })

    Handlebars.registerHelper('getCustomField1FType', function () {
        const thisType = game.settings.get('od6s', 'custom_field_1_type')
        if (thisType === "string") {
            return "String";
        }

        if (thisType === "number") {
            return "Number";
        }
    })

    Handlebars.registerHelper('getItemsByType', async function (type) {
        return await od6sutilities.getItemsFromCompendiumByType(type);
    })

    Handlebars.registerHelper('actionsCount', async function (actor) {
        return (actor.actions.length);
    })

    Handlebars.registerHelper('setDice', function (dice, actionPenalty, woundPenalty, otherPenalty) {
        let newDice = (+dice) - (+actionPenalty) - (+woundPenalty) - (+otherPenalty);
        if (newDice <= 0) {
            newDice = 0;
        }
        return newDice;
    })

    Handlebars.registerHelper('getAttributeName', function (attribute) {
        return getAttributeName(attribute);
    })

    Handlebars.registerHelper('getAttributeShortName', function (attribute) {
        return getAttributeShortName(attribute);
    })

    Handlebars.registerHelper('getAttributes', function () {
        return OD6S.attributes;
    })

    Handlebars.registerHelper('getRanges', function () {
        return OD6S.ranges;
    })

    Handlebars.registerHelper('getDifficulties', function () {
        return OD6S.difficulty;
    })

    Handlebars.registerHelper('isDefense', function (value) {
        if (value === 'dodge' || value === 'parry' || value === 'block') {
            return true;
        } else {
            return false;
        }
    })

    Handlebars.registerHelper('getCover', function (type) {
        return OD6S.cover[type];
    })

    Handlebars.registerHelper('getCalledShot', function () {
        return OD6S.calledShot;
    })

    Handlebars.registerHelper('getGravity', function () {
        return OD6S.gravity;
    })

    Handlebars.registerHelper('isGmOrOwner', function (id) {
        if (game.user.isGM) return true;
        return game.actors.find(a => a.id === id).isOwner;
    })

    Handlebars.registerHelper('getUseFatePoint', function () {
        return game.i18n.localize(OD6S.useAFatePointName);
    })

    Handlebars.registerHelper('isCardVisible', function (message) {
        if (game.user.isGM) return true;
        return message.flags.od6s.isVisible;
    })

    Handlebars.registerHelper('isHideDifficulty', function () {
        return game.settings.get('od6s', 'roll-modifiers');
    })

    Handlebars.registerHelper('isHideAllRolls', function () {
        return !game.settings.get('od6s', 'roll-modifiers');
    })

    Handlebars.registerHelper('isKnown', function (isKnown) {
        if (game.user.isGM) return true;
        return isKnown;
    })

    Handlebars.registerHelper('getChatTemplate', function (messageId) {
        const messageType = game.messages.get(messageId).getFlag('od6s', 'type')
        switch (messageType) {
            case "opposed":
                return OD6S.chatTemplates.opposed;
            case "damageresult":
                return OD6S.chatTemplates.damageresult;
            case "attribute":
            case "skill":
            case "specialization":
            case "weapon":
            case "action":
            case "damage":
            case "resistance":
                return OD6S.chatTemplates.roll;
            default:
                return OD6S.chatTemplates.generic;
        }
    })

    Handlebars.registerHelper('getCyberneticsLocations', () => {
        return OD6S.cyberneticsLocations;
    })

    Handlebars.registerHelper('getCybernetics', function (actor, location) {
        return actor.items.filter(i => i.type === "cybernetic" && i.data.data.location === location);
    })

    Handlebars.registerHelper('hasCybernetics', function (actor) {
        return actor.items.filter(i => i.type === "cybernetic").length;
    })

    Handlebars.registerHelper('getManifestations', function (actor) {
        return actor.items.filter(i => i.type === "manifestation");
    })

    Handlebars.registerHelper('getConfig', function (key, subKey) {
        if (subKey) {
            return OD6S[key][subKey];
        }
        return OD6S[key];
    })

    loadHandleBarTemplates();

});

async function loadHandleBarTemplates() {
    const charTabPath = 'systems/od6s/templates/actor/character/tabs/';
    const commonTabPath = 'systems/od6s/templates/actor/common/tabs/';
    const npcTabPath = 'systems/od6s/templates/actor/npc/tabs/';
    const creatureTabPath = 'systems/od6s/templates/actor/creature/tabs/';
    const chatPath = 'systems/od6s/templates/chat/'
    const templatePaths = [
        charTabPath + "biography.html",
        charTabPath + "attributes.html",
        charTabPath + "inventory.html",
        charTabPath + "metaphysics.html",
        commonTabPath + "attribute-column.html",
        commonTabPath + "cybernetics.html",
        commonTabPath + "special-abilities.html",
        commonTabPath + "combat.html",
        commonTabPath + "data.html",
        npcTabPath + "main.html",
        creatureTabPath + "main.html",
        chatPath + "generic.html",
        chatPath + "roll.html",
        chatPath + "opposed.html",
        chatPath + "damageresult.html",
        "systems/od6s/templates/item/item-effects.html",
        "systems/od6s/templates/item/item-labels-tags.html"
    ]
    return loadTemplates(templatePaths);
}

// Chat listeners
Hooks.on('renderChatMessage', (msg, html, data) => {
    if (game.settings.get('od6s', 'hide-gm-rolls') && data.whisperTo !== '') {
        if (game.user.isGM === false &&
            game.user.data.id !== data.author.id &&
            data.message.whisper.indexOf(game.user.id) === -1) {
            msg.data.sound = null;
            html.hide();
        }
    }
})

Hooks.on("updateChatMessage", (message, data, diff, id) => {
    if (data.blind === false) {
        let messageLi = $(`.message[data-message-id=${data._id}]`);
        messageLi.show();
    }
});

Hooks.on('renderChatLog', (log, html, data) => {
    html.on("click", ".modifiers-button", async ev => {
        let content = document.getElementById("modifiers-display-" + ev.currentTarget.dataset.messageId);
        if (content.style.display === "block") {
            content.style.display = "none";
        } else {
            content.style.display = "block";
        }
        game.messages.get(ev.currentTarget.dataset.messageId).render();
    })

    html.on("click", ".damage-modifiers-button", async ev => {
        let content = document.getElementById("damage-modifiers-display-" + ev.currentTarget.dataset.messageId);
        if (content.style.display === "block") {
            content.style.display = "none";
        } else {
            content.style.display = "block";
        }
        game.messages.get(ev.currentTarget.dataset.messageId).render();
    })

    html.on("click", ".damage-button", async ev => {
        ev.preventDefault();
        const data = ev.currentTarget.dataset;

        const rollScore = (+data.damage) - 3;
        const dice = od6sutilities.getDiceFromScore(rollScore);
        let rollString = dice.dice + "d6" + game.i18n.localize('OD6S.BASE') + "+1dw";
        dice.pips ? rollString += " + " + dice.pips : null;

        let roll = await new Roll(rollString).evaluate({"async": true});

        const label = game.i18n.localize('OD6S.DAMAGE') + " (" +
            game.i18n.localize(OD6S.damageTypes[data.damagetype]) + ")";

        const flags = {
            "type": "damage",
            "damageType": data.damagetype,
            "attackMessage": data.messageId,
            "isOpposable": true,
            "total": roll.total
        }

        await roll.toMessage({
            speaker: ChatMessage.getSpeaker({actor: game.actors.find(a => a.id === data.actor)}),
            flavor: label,
            flags: {
                od6s: flags
            }
        });
    })

    html.on("click", ".edit-difficulty", async ev => {
        ev.preventDefault();
        let data = {};
        const dataSet = ev.currentTarget.dataset;
        data.messageId = dataSet.messageId;
        const message = game.messages.get(dataSet.messageId);
        data.baseDifficulty = message.getFlag('od6s', 'baseDifficulty');
        data.modifiers = message.getFlag('od6s', 'modifiers');
        new OD6SEditDifficulty(data).render(true);
    })

    html.on("click", ".edit-damage", async ev => {
        ev.preventDefault();
        let data = {};
        const dataSet = ev.currentTarget.dataset;
        data.messageId = dataSet.messageId;
        const message = game.messages.get(dataSet.messageId);
        data.damage = message.getFlag('od6s', 'damageScore');
        new OD6SEditDamage(data).render(true);
    })

    html.on("click", ".choose-target", async ev => {
        ev.preventDefault();
        data = {};
        data.targets = [];
        data.messageId = ev.currentTarget.dataset.messageId;
        if (game.user.isGM) {
            // If in combat, only load tokens in combat.  Otherwise, load all tokens in scene
            if (game.combat) {
                for (let t of game.combat.combatants) {
                    const target = {
                        "id": t.token.id,
                        "name": t.token.name
                    }
                    data.targets.push(target);

                }
            } else {
                data.targets = game.scenes.active.data.tokens;
            }
        } else {
            return;
        }
        new OD6SChooseTarget(data).render(true);
    })

    html.on("click", ".wilddiegm", async ev => {
        ev.preventDefault();
        // three choices: leave it as-is, remove the highest die from the roll, or cause a complication
        new OD6SHandleWildDieForm(ev).render(true);
    })

    html.on("click", ".message-reveal", async ev => {
        const message = game.messages.get(ev.currentTarget.dataset.messageId);
        await message.setFlag('od6s', 'isVisible', true);
        await message.setFlag('od6s', 'isKnown', true);
    })

    html.on("click", ".message-oppose", async ev => {
        ev.preventDefault();
        // Check if there are any opposed cards already in the pipe
        if (OD6S.opposed.length > 0) {
            OD6S.opposed.push(ev.currentTarget.dataset.messageId);
            return handleOpposedRoll();
        } else {
            OD6S.opposed.push(ev.currentTarget.dataset.messageId);

        }
    })
})

async function handleOpposedRoll() {
    let type = '';
    let winner = '';
    let loser = '';
    let diff = 0;
    let result = '';
    let data = {};
    data.flags = {};
    const message1 = game.messages.get(OD6S.opposed[0]);
    const message2 = game.messages.get(OD6S.opposed[1]);
    OD6S.opposed = [];

    if ((message1.getFlag('od6s', 'type') === 'damage' && message2.getFlag('od6s', 'type') === 'resistance') ||
        (message1.getFlag('od6s', 'type') === 'resistance' && message2.getFlag('od6s', 'type') === 'damage')) {
        type = "damageresult";
    } else {
        type = "opposedcheck";
    }

    if (message1.roll.total > message2.roll.total) {
        winner = message1;
        loser = message2;
    } else {
        winner = message2;
        loser = message1;
    }

    diff = (+winner.roll.total) - (+loser.roll.total);
    if (type === "damageresult") {
        if (winner.getFlag('od6s', 'type') === "damage") {
            data.content = winner.alias + " " + game.i18n.localize("OD6S.INJURES") + " " + loser.alias;
            result = getInjury(diff);
        } else {
            data.content = winner.alias + " " + game.i18n.localize("OD6S.RESISTS") + " " + loser.alias;
            result = 'OD6S.NO_INJURY';
        }
    } else {
        data.flavor = message1.alias + " " + game.i18n.localize("OD6S.VS") + " " + message2.alias;
        data.content = winner.alias + " " + game.i18n.localize("OD6S.WINS");
    }

    data.flags.od6s = {
        "isOpposed": true,
        "type": type,
        "isVisible": false,
        "result": result
    }

    await ChatMessage.create(data);
}

function getInjury(damage) {
    let resultMessage = '';
    for (let result in OD6S.damage) {
        if (damage >= OD6S.damage[result]) {
            resultMessage = result;
        } else {
            break;
        }
    }
    return resultMessage;
}

// Custom dice for DiceSoNice
Hooks.on('diceSoNiceReady', (dice3d) => {
    dice3d.addSystem({id: 'od6s', name: "OpenD6 Space"})
    dice3d.addDicePreset({
        type: "dw",
        labels: [game.settings.get('od6s', 'wild_die_one_face'), "2", "3", "4", "5", game.settings.get('od6s', 'wild_die_six_face')],
        colorset: "white",
        values: {min: 1, max: 6},
        system: "od6s"
    }, "d6")
})

Hooks.on('diceSoNiceRollStart', (messageId, context) => {
    const roll = context.roll;
    let die;
    let len = roll.dice.length;
    // Customize colors for Dice So Nice
    for (die = 0; die < len; die++) {
        switch (roll.dice[die].options.flavor) {
            case "Wild":
                roll.dice[die].options.colorset = "white";
                break;
            case "CP":
                roll.dice[die].options.colorset = "bronze";
                break;
            case "Bonus":
                roll.dice[die].options.colorset = "black";
                break;
            default:
                break;
        }
    }
})

Hooks.on('deleteActiveEffect', async (effect) => {
    for (let change of effect.data.changes) {
        if (change.key.startsWith("data.items")) {
            const t = change.key.split('.');
            if (t[2] === 'skills' || t[2] === 'skill' || t[2] === 'specializations' || t[2] === 'specialization') {
                const item = effect.parent.items.find(i => i.name === t[3]);
                if (typeof (item) !== 'undefined') {
                    const update = {};
                    update.id = item.id;
                    update.data = {};
                    update.data[t[4]] = {};
                    if (change.mode === 2) {
                        update.data[t[4]] -= (+change.value);
                        item.update(update);
                    }
                }
            } else if (t[2] === 'weapon' || t[2] === 'weapons') {
                const item = effect.parent.items.find(i => i.name === t[3]);
                if (typeof (item) !== 'undefined') {
                    const update = {};
                    update.id = item.id;
                    update.data = {};
                    if (t[4] === 'mods') {
                        update.data[t[4]] = {};
                        if (change.mode === 2) {
                            update.data[t[4]][t[5]] -= (+change.value);
                            item.update(update);
                        }
                    } else {
                        if (change.mode === 2) {
                            update.data[t[4]] -= (+change);
                            item.update(update);
                        }
                    }
                }
            }
        }
    }
})

Hooks.on("getOD6SChatLogEntryContext", async (html, options) => {
    await OD6SChat.chatContextMenu(html, options);
})

Hooks.once("ready", async function () {
    // Wait to register hotbar drop hook on ready so that modules could register earlier if they want to
    Hooks.on("hotbarDrop", (bar, data, slot) => createOD6SMacro(data, slot));

    // Set customizations
    OD6S.fatePointsName = game.settings.get('od6s', 'customize_fate_points') ?
        game.settings.get('od6s', 'customize_fate_points') : game.i18n.localize('OD6S.CHAR_FATE_POINTS');
    OD6S.fatePointsShortName = game.settings.get('od6s', 'customize_fate_points_short') ?
        game.settings.get('od6s', 'customize_fate_points_short') : game.i18n.localize('OD6S.CHAR_FATE_POINTS_SHORT');

    OD6S.manifestationsName = game.settings.get('od6s', 'customize_manifestations') ?
        game.settings.get('od6s', 'customize_manifestations') : game.i18n.localize('OD6S.CHAR_MANIFESTATIONS');

    OD6S.manifestationName = game.settings.get('od6s', 'customize_manifestation') ?
        game.settings.get('od6s', 'customize_manifestation') : game.i18n.localize('OD6S.CHAR_MANIFESTATION');

    OD6S.metaphysicsExtranormalName = game.settings.get('od6s', 'customize_metaphysics_extranormal') ?
        game.settings.get('od6s', 'customize_metaphysics_extranormal') : game.i18n.localize('OD6S.CHAR_METAPHYSICS_EXTRANORMAL');
    OD6S.useAFatePointName = game.settings.get('od6s', 'customize_use_a_fate_point') ?
        game.settings.get('od6s', 'customize_use_a_fate_point') : game.i18n.localize('OD6S.USE_FATE_POINT');

    OD6S.attributes.agi.name = game.settings.get('od6s', 'customize_agility_name') ?
        game.settings.get('od6s', 'customize_agility_name') : game.i18n.localize('OD6S.CHAR_AGILITY');
    OD6S.attributes.agi.shortName = game.settings.get('od6s', 'customize_agility_name_short') ?
        game.settings.get('od6s', 'customize_agility_name_short') : game.i18n.localize('OD6S.CHAR_AGILITY_SHORT');

    OD6S.attributes.str.name = game.settings.get('od6s', 'customize_strength_name') ?
        game.settings.get('od6s', 'customize_strength_name') : game.i18n.localize('OD6S.CHAR_STRENGTH');
    OD6S.attributes.str.shortName = game.settings.get('od6s', 'customize_strength_name_short') ?
        game.settings.get('od6s', 'customize_strength_name_short') : game.i18n.localize('OD6S.CHAR_STRENGTH_SHORT');

    OD6S.attributes.mec.name = game.settings.get('od6s', 'customize_mechanical_name') ?
        game.settings.get('od6s', 'customize_mechanical_name') : game.i18n.localize('OD6S.CHAR_MECHANICAL');
    OD6S.attributes.mec.shortName = game.settings.get('od6s', 'customize_mechanical_name_short') ?
        game.settings.get('od6s', 'customize_mechanical_name_short') : game.i18n.localize('OD6S.CHAR_MECHANICAL_SHORT');

    OD6S.attributes.kno.name = game.settings.get('od6s', 'customize_knowledge_name') ?
        game.settings.get('od6s', 'customize_knowledge_name') : game.i18n.localize('OD6S.CHAR_KNOWLEDGE');
    OD6S.attributes.kno.shortName = game.settings.get('od6s', 'customize_knowledge_name_short') ?
        game.settings.get('od6s', 'customize_knowledge_name_short') : game.i18n.localize('OD6S.CHAR_KNOWLEDGE_SHORT');

    OD6S.attributes.per.name = game.settings.get('od6s', 'customize_perception_name') ?
        game.settings.get('od6s', 'customize_perception_name') : game.i18n.localize('OD6S.CHAR_PERCEPTION');
    OD6S.attributes.per.shortName = game.settings.get('od6s', 'customize_perception_name_short') ?
        game.settings.get('od6s', 'customize_perception_name_short') : game.i18n.localize('OD6S.CHAR_PERCEPTION_SHORT');

    OD6S.attributes.tec.name = game.settings.get('od6s', 'customize_technical_name') ?
        game.settings.get('od6s', 'customize_technical_name') : game.i18n.localize('OD6S.CHAR_TECHNICAL');
    OD6S.attributes.tec.shortName = game.settings.get('od6s', 'customize_technical_name_short') ?
        game.settings.get('od6s', 'customize_technical_name_short') : game.i18n.localize('OD6S.CHAR_TECHNICAL_SHORT');

    OD6S.attributes.met.name = game.settings.get('od6s', 'customize_metaphysics_name') ?
        game.settings.get('od6s', 'customize_metaphysics_name') : game.i18n.localize('OD6S.CHAR_METAPHYSICS');
    OD6S.attributes.met.shortName = game.settings.get('od6s', 'customize_metaphysics_name_short') ?
        game.settings.get('od6s', 'customize_metaphysics_name_short') : game.i18n.localize('OD6S.CHAR_METAPHYSICS_SHORT');

    game.i18n.translations.ITEM.TypeManifestation = OD6S.manifestationName;

    if (game.settings.get('od6s', 'parry_skills')) {
        OD6S.actions.parry.skill = "OD6S.MELEE_PARRY",
            OD6S.actions.block.skill = "OD6S.BRAWLING_PARRY";
        OD6S.actions.block.name = "OD6S.BRAWLING_PARRY";
    } else {
        OD6S.actions.parry.skill = "OD6S.MELEE_COMBAT";
        OD6S.actions.block.skill = "OD6S.BRAWL";
        OD6S.actions.block.name = "OD6S.ACTION_BRAWL_BLOCK"
    }
    OD6S.opposed = [];
});

Hooks.on("updateCombat", async (Combat, data) => {
    if (game.user.isGM && Combat.data.round === 1 && Combat.data.turn === 0 && Combat.data.active && OD6S.startCombat) {
        // At the start of a new combat, make sure all actor's action lists are cleared
        OD6S.startCombat = false;
        for (let i = 0; i < Combat.combatants.length; i++) {
            await clearActionList(Combat.combatants[i].actor);
        }
    }

    if (game.user.isGM && Combat.data.round !== 0 && Combat.data.turn === 0 && Combat.data.active && !OD6S.startCombat) {
        // New round
    }

    // Actor has started their turn, clear their action list and defensive bonuses/penalties
    if (typeof (Combat.combatant.actor) !== 'undefined') {
        if (!game.settings.get('od6s', 'reaction_skills')) {
            const update = {};
            update.id = Combat.combatant.actor.id;
            update.data = {};
            update.data.parry = {};
            update.data.parry.score = 0;
            update.data.dodge = {};
            update.data.dodge.score = 0;
            update.data.block = {};
            update.data.block.score = 0;
            await Combat.combatant.actor.update(update, {'diff': true});
        }
        await Combat.combatant.actor.setFlag('od6s', 'fatepointeffect', false);
    }
})

Hooks.on("preUpdateCombat", async (Combat, data) => {
    // End-of-turn stuff here
    if (data.turn === 0) {
        for (let i = 0; i < Combat.combatants.size; i++) {
            const combatant = Combat.combatants.contents[i].actor;
            if (typeof (combatant) !== 'undefined') {
                await clearActionList(combatant);
                if (game.settings.get('od6s', 'reaction_skills')) {
                    const update = {};
                    update.id = combatant.id;
                    update.data = {};
                    update.data.parry = {};
                    update.data.parry.score = 0;
                    update.data.dodge = {};
                    update.data.dodge.score = 0;
                    update.data.block = {};
                    update.data.block.score = 0;
                    await combatant.update(update, {'diff': true});
                }
            }
        }
    }
})

Hooks.on("createCombat", async (Combat, combatant, info, data) => {
    if (!game.user.isGM) {
        return;
    }
    OD6S.startCombat = true;
})

Hooks.on("deleteCombat", async function (Combat) {
    // Combat is over, clear all combatant action lists
    for (let i = 0; i < Combat.combatants.length; i++) {
        await clearActionList(Combat.combatants[i].actor);
    }
})

async function clearActionList(actor) {
    if (actor !== null) {
        const actions = actor.itemTypes.action;
        for (let i = 0; i < actions.length; i++) {
            await actor.deleteOwnedItem(actions[i].id);
        }
    }
}

/* -------------------------------------------- */
/*  Hotbar Macros                               */

/* -------------------------------------------- */

/**
 * Create a Macro from an Item drop.
 * Get an existing item macro if one exists, otherwise create a new one.
 * @param {Object} data     The dropped data
 * @param {number} slot     The hotbar slot to use
 * @returns {Promise}
 */
async function createOD6SMacro(data, slot) {
    if (data.type !== "Item") return;
    if (!("data" in data)) return ui.notifications.warn(game.i18n.localize('OD6S.WARN_NOT_OWNED'));
    const item = data.data;

    // Filter out certain item types
    if (item.type === '' ||
        item.type === 'charactertemplate' ||
        item.type === 'action' ||
        item.type === 'disadvantage' ||
        item.type === 'advantage' ||
        item.type === 'armor' ||
        item.type === 'gear' ||
        item.type === 'cybernetic' ||
        item.type === 'vehicle') {
        return ui.notifications.warn(game.i18n.localize('OD6S.WARN_INVALID_MACRO_ITEM'));
    }

    // Create the macro command
    const command = `game.od6s.rollItemMacro("${item._id}");`;
    let macro = game.macros.entities.find(m => (m.name === item.name) && (m.command === command));
    if (!macro) {
        macro = await Macro.create({
            name: item.name,
            type: "script",
            img: item.img,
            command: command,
            flags: {"od6s.itemMacro": true}
        });
    }
    await game.user.assignHotbarMacro(macro, slot);
    return false;
}

/**
 * Create a Macro from an Item drop.
 * Get an existing item macro if one exists, otherwise create a new one.
 * @param {string} itemId
 * @return {Promise}
 */
export function rollItemMacro(itemId) {
    const speaker = ChatMessage.getSpeaker();
    let actor;
    if (speaker.token) actor = game.actors.tokens[speaker.token];
    if (!actor) actor = game.actors.get(speaker.actor);
    const item = actor ? actor.items.find(i => i.id === itemId) : null;
    if (!item) return ui.notifications.warn(`Your controlled Actor does not have an item id of ${itemId}`);

    // Trigger the item roll
    return item.roll();
}

/**
 * Return either the customized or translated name of an attribute
 * @param attribute
 * @returns {string}
 */
export function getAttributeName(attribute) {
    attribute = attribute.toLowerCase();
    return OD6S.attributes[attribute].name;
}

export function getAttributeShortName(attribute) {
    attribute = attribute.toLowerCase();
    return OD6S.attributes[attribute].shortName;
}

async function simpleRoll() {
    const html = await renderTemplate("systems/od6s/templates/simpleRoll.html",
        {"wilddie": true, "dice": 1, "pips": 0});
    new Dialog({
        title: game.i18n.localize('OD6S.ROLL'),
        content: html,
        buttons: {
            roll: {
                label: game.i18n.localize('OD6S.ROLL'),
                callback: async (dlg) => {
                    let rollString = "";
                    let rollMode = 0;
                    let dice = $(dlg[0]).find("#dice")[0].value
                    let pips = $(dlg[0]).find("#pips")[0].value;
                    const wild = $(dlg[0]).find("#wilddie")[0].checked;
                    if (wild) {
                        dice -= 1;
                        if (dice < 0) {
                            ui.notifications.warn('OD6S.NOT_ENOUGH_DICE');
                            return;
                        }
                        if (dice > 0) rollString = dice + 'd6' + game.i18n.localize('OD6S.BASE_DIE_FLAVOR');
                        rollString += '+1dw' + game.i18n.localize('OD6S.WILD_DIE_FLAVOR');
                    } else {
                        rollString = dice + 'd6' + game.i18n.localize('OD6S.BASE_DIE_FLAVOR');
                    }
                    if (pips > 0) rollString += '+' + pips;
                    let roll = await new Roll(rollString).evaluate({"async": true});
                    let label = `${game.i18n.localize('OD6S.ROLLING')} ${rollString}`;

                    if (game.user.isGM && game.settings.get('od6s', 'hide-gm-rolls')) rollMode = CONST.DICE_ROLL_MODES.PRIVATE;
                    await roll.toMessage({
                            speaker: ChatMessage.getSpeaker(),
                            flavor: label
                        },
                        {rollMode: rollMode}
                    );
                }
            }
        }
    }).render(true);
}

export class WildDie extends Die {
    constructor(termData) {
        termData.faces = 6;
        termData.modifiers = ["x6"];
        super(termData);
    }

    static DENOMINATION = "w";
}

export class CharacterPointDie extends Die {
    constructor(termData) {
        termData.faces = 6;
        termData.modifiers = ["x6"];
        super(termData);
    }

    static DENOMINATION = "b";
}

