import {od6sutilities} from "./utilities.js";
import OD6S from "./config/config-od6s.js";

export class InitRollDialog extends Dialog {
    constructor(caller, data, options) {
        super(data, options);
        this.rollData = caller.rollData;
        this.cpLimit = 5;
    }

    activateListeners(html) {
        super.activateListeners(html);

        html.find('.cpup').click(async () => {
            if ((+this.rollData.characterpoints) >= this.cpLimit) {
                ui.notifications.warn(game.i18n.localize("OD6S.MAX_CP"));
            } else if ((+this.rollData.characterpoints) >= this.rollData.actor.data.data.characterpoints.value) {
                ui.notifications.warn(game.i18n.localize("OD6S.NOT_ENOUGH_CP_ROLL"));
            } else {
                this.rollData.characterpoints++;
                await this.updateDialog();
            }
        });

        html.find('.bonusdice').change(async ev => {
            this.rollData.bonusdice = (+ev.target.valueAsNumber);
            //this.rollData.bonusdice = ev.currentTarget.dataset.bonusdice;
        })

        html.find('.bonuspips').change(async ev => {
            this.rollData.bonuspips = (+ev.target.valueAsNumber);
        })

        html.find('.cpdown').click(async () => {
            if (this.rollData.characterpoints > 0) {
                this.rollData.characterpoints--;
            }
            await this.updateDialog();
        });

        html.find('.usewilddie').click(async () => {
            this.rollData.wilddie = !Boolean(this.rollData.wilddie);
            await this.updateDialog();
        });

    }

    async updateDialog() {
        this.rollData.characterpoints > this.rollData.actor.data.data.characterpoints.value ? this.rollData.cpcostcolor = "red" :
            this.rollData.cpcostcolor = "black";
        const advanceTemplate = "systems/od6s/templates/initRoll.html";
        this.data.content = await renderTemplate(advanceTemplate, this.rollData);
        this.render();
    }
}

export class od6sInitRoll {

    activateListeners(html) {
        super.activateListeners(html);
    }

    static async _onInitRollDialog(combat, combatant) {
        const combatantId = combatant.id;
        const actor = combatant.actor;
        const actorData = actor.data.data;
        let initScore = actorData.initiative.score;
        const dice = od6sutilities.getDiceFromScore(initScore).dice;
        const pips = od6sutilities.getDiceFromScore(initScore).pips;

        this.rollData = {
            label: game.i18n.localize('OD6S.INITIATIVE'),
            title: game.i18n.localize('OD6S.INITIATIVE'),
            dice: dice,
            pips: pips,
            wilddie: game.settings.get('od6s','use_wild_die'),
            showWildDie: game.settings.get('od6s','use_wild_die'),
            characterpoints: 0,
            cpcost: 0,
            cpcostcolor: "black",
            bonusdice: 0,
            bonuspips: 0,
            actor: actor,
            combat: combat,
            combatantId: combatantId
        }

        let template = "systems/od6s/templates/initRoll.html";
        const html = await renderTemplate(template, this.rollData);

        new InitRollDialog(this, {
            title: game.i18n.localize("OD6S.ROLL") + "!",
            content: html,
            buttons: {
                submit: {
                    label: game.i18n.localize("OD6S.ROLL"),
                    callback: () => od6sInitRoll.initRollAction(
                        this
                    )
                }
            }
        }).render(true);
    }

    static async initRollAction(caller) {
        let rollString;
        let cpString;
        const rollData = caller.rollData;

        // Wild die explodes on a 6
        if (rollData.wilddie) {
            rollData.dice = (+rollData.dice) - 1;
            rollString = rollData.dice;
            rollString += "d6" + game.i18n.localize("OD6S.BASE_DIE_FLAVOR") + "+1dw" +
                game.i18n.localize("OD6S.WILD_DIE_FLAVOR") ;
        } else {
            rollString = rollData.dice + "d6" + game.i18n.localize("OD6S.BASE_DIE_FLAVOR");
        }

        if (rollData.pips > 0) {
            rollString += "+" + rollData.pips;
        }

        // Character point dice also explode on a 6
        if (rollData.characterpoints > 0) {
            cpString = "+" + rollData.characterpoints + "db" +
                game.i18n.localize("OD6S.CHARACTER_POINT_DIE_FLAVOR");
            rollString += cpString;
        }

        // Bonus pips are not calculated to add new dice, just a bonus
        if (rollData.bonusdice > 0) {
            rollString += "+" + rollData.bonusdice + "d6" + game.i18n.localize("OD6S.BONUS_DIE_FLAVOR")
        }
        if (rollData.bonuspips > 0) {
            rollString += "+" + rollData.bonuspips;
        }

        // Add fraction of AGI and mods to break ties
        const fraction = (+rollData.actor.data.data.attributes.per.score) * 0.01 +
            (+rollData.actor.data.data.initiative.mod) * 0.01 +
            (+rollData.actor.data.data.attributes.agi.score) * 0.01;
        rollString = rollString + "+" + fraction;

        // Apply costs
        if ((rollData.characterpoints > 0) && (rollData.actor.data.data.characterpoints.value > 0)) {
            const update = {};
            update.data = {};
            update.data.characterpoints = {};
            update.id = rollData.actor.id;
            update.data.characterpoints.value = rollData.actor.data.data.characterpoints.value -= rollData.characterpoints;
            await rollData.actor.update(update, {diff: true});
        }

        const messageOptions = {};
        if (game.user.isGM && game.settings.get('od6s', 'hide-gm-rolls')) messageOptions.rollMode = CONST.DICE_ROLL_MODES.PRIVATE;
        await game.combats.active.rollInitiative(rollData.combatantId, {"formula": rollString, "messageOptions": messageOptions});
    }
}

export class RollDialog extends Dialog {

    constructor(actorSheet, data, options) {
        super(data, options);
        this.actorSheet = actorSheet;
        this.rollData = this.actorSheet.rollData;

        this.cpLimit = {
            skill: 2,
            attribute: 2,
            specialization: 5,
            dodge: 5,
            parry: 5,
            block: 5,
            dr: 5
        }
    }

    activateListeners(html) {

        super.activateListeners(html);

        html.find('.cpup').click(async () => {
            let rollType = this.rollData.type;

            if (this.rollData.type === "skill") {
                // Check if it is a dodge or parry
                if (this.rollData.title.includes("Parry")) {
                    rollType = "parry";
                } else if (this.rollData.title.includes("Dodge")) {
                    rollType = "dodge";
                } else if (this.rollData.title.includes("Block")) {
                    rollType = "block";
                }
            }

            if ((+this.rollData.characterpoints) >= this.cpLimit[rollType]) {
                ui.notifications.warn(game.i18n.localize("OD6S.MAX_CP"));
            } else if ((+this.rollData.characterpoints) >= this.rollData.actor.data.data.characterpoints.value) {
                ui.notifications.warn(game.i18n.localize("OD6S.NOT_ENOUGH_CP_ROLL"));
            } else {
                this.rollData.characterpoints++;
                await this.updateDialog();
            }
        });

        html.find('.useattribute').change(async ev => {
            // Player has changed the underlying attribute to use for the roll, recalculate
            this.rollData.attribute = ev.target.value;
            const attributeScore = this.rollData.actor.data.data.attributes[ev.target.value].score;
            const skillScore = this.rollData.actor.items.filter(i => i.name === this.rollData.label)[0].data.data.score;
            const newScore = (+attributeScore) + (+skillScore);
            const newDice = od6sutilities.getDiceFromScore(newScore);
            this.rollData.dice = newDice.dice;
            this.rollData.pips = newDice.pips;
            await this.updateDialog();
        })

        html.find('.bonusdice').change(async ev => {
            this.rollData.bonusdice = (+ev.target.valueAsNumber);
            //this.rollData.bonusdice = ev.currentTarget.dataset.bonusdice;
        })

        html.find('.bonuspips').change(async ev => {
            this.rollData.bonuspips = (+ev.target.valueAsNumber);
        })

        html.find('.cpdown').click(async () => {
            if (this.rollData.characterpoints > 0) {
                this.rollData.characterpoints--;
            }
            await this.updateDialog();
        });

        html.find('.usefatepoint').click(async () => {
            this.rollData.fatepoint = !Boolean(this.rollData.fatepoint);
            if (this.rollData.fatepoint && (this.rollData.actor.data.data.fatepoints.value <= 0)) {
                ui.notifications.warn(game.i18n.localize("OD6S.NOT_ENOUGH_FP_ROLL"));
                this.rollData.fatepoint = !Boolean(this.rollData.fatepoint);
            }
            if (this.rollData.fatepoint) {
                this.rollData.dice = this.rollData.dice * 2;
                this.rollData.pips = this.rollData.pips * 2;
            } else {
                this.rollData.dice = this.rollData.originaldice;
                this.rollData.pips = this.rollData.originalpips;
            }
            await this.updateDialog();
        });

        html.find('.usewilddie').click(async () => {
            this.rollData.wilddie = !Boolean(this.rollData.wilddie);
            await this.updateDialog();
        });

        html.find('.fulldefense').click(async () => {
            this.rollData.fulldefense = !Boolean(this.rollData.fulldefense);
            await this.updateDialog();
        });

        html.find('.difficulty').change(async (ev) => {
            this.rollData.difficulty = (+ev.target.valueAsNumber);
            await this.updateDialog();
        })

        html.find('.actionpenalty').change(async (ev) => {
            this.rollData.actionpenalty = (+ev.target.valueAsNumber);
            await this.updateDialog();
        })

        html.find('.woundpenalty').change(async (ev) => {
            this.rollData.woundpenalty = (+ev.target.valueAsNumber);
            await this.updateDialog();
        })

        html.find('.otherpenalty').change(async (ev) => {
            this.rollData.otherpenalty = (+ev.target.valueAsNumber);
            await this.updateDialog();
        })

        html.find('.shots').change(async (ev) => {
            this.rollData.shots = (+ev.target.valueAsNumber);
            await this.updateDialog();
        })

        html.find('.target').change(async (ev) => {
            this.rollData.target = ev.target.value;
            await this.updateDialog();
        })

        html.find('.difficultylevel').change(async (ev) => {
            this.rollData.difficultylevel = ev.target.value;
            await this.updateDialog();
        })

        html.find('.range').change(async (ev) => {
            this.rollData.modifiers.range = ev.target.value;
            await this.updateDialog();
        })

        html.find('.attackoption').change(async (ev) => {
            this.rollData.multishot = ev.target.value === 'OD6S.ATTACK_RANGED_SINGLE_FIRE_AS_MULTI';
            this.rollData.modifiers.attackoption = ev.target.value;
            await this.updateDialog();
        })

        html.find('.calledshot').change(async (ev) => {
            this.rollData.modifiers.calledshot = ev.target.value;
            await this.updateDialog();
        })

        html.find('.cover').change(async (ev) => {
            this.rollData.modifiers.cover = ev.target.value;
            await this.updateDialog();
        })

        html.find('.coverlight').change(async (ev) => {
            this.rollData.modifiers.coverlight = ev.target.value;
            await this.updateDialog();
        })

        html.find('.coversmoke').change(async (ev) => {
            this.rollData.modifiers.coversmoke = ev.target.value;
            await this.updateDialog();
        })

        html.find('.miscmod').change(async (ev) => {
            this.rollData.modifiers.miscmod = ev.target.value;
            await this.updateDialog();
        })
    }

    async updateDialog() {
        if (this.rollData.actor.data.type === 'character') {
            this.rollData.characterpoints > this.rollData.actor.data.data.characterpoints.value ? this.rollData.cpcostcolor = "red" :
                this.rollData.cpcostcolor = "black";
        }
        const rollTemplate = "systems/od6s/templates/roll.html";
        this.data.content = await renderTemplate(rollTemplate, this.rollData);
        this.render();
    }
}

export class od6sroll {

    activateListeners(html) {
        super.activateListeners(html);
    }

    async _onRollItem(event) {
        const item = this.actor.items.find(i => i.id === event.currentTarget.dataset.itemId);
        return item.roll();
    }

    async _onRollEvent(event) {
        event.preventDefault();
        const eventData = {};
        const element = event.currentTarget;
        const dataset = element.dataset;

        eventData.name = dataset.label;
        eventData.score = dataset.score;
        eventData.type = dataset.type;
        eventData.actor = this.actor;
        eventData.itemId = dataset.itemId ? dataset.itemId : ""

        await od6sroll._onRollDialog(eventData);
    }

    static async _onRollDialog(data) {
        let attribute;
        let range = "OD6S.RANGE_POINT_BLANK_SHORT";
        let woundPenalty = 0;
        let damageType = '';
        let damageScore = 0;
        let damageModifiers = [];
        let targets = [];
        let difficulty = 0;
        let isVisible = false;
        let isOpposable = false;
        let isKnown = false;
        let bonusmod = 0;
        let bonusdice = 0;
        let miscMod = 0;

        if (typeof (data.difficulty) !== 'undefined') {
            difficulty = data.difficulty;
        }
        if (data.actor.data.data.sheetmode.value !== "normal") {
            ui.notifications.warn(game.i18n.localize("OD6S.WARN_SHEET_MODE_NOT_NORMAL"));
            return;
        }

        if (data.subtype === "Ranged") {
            data.subtype = "rangedattack"
        }
        if (data.subtype === "Melee") {
            data.subtype = "meleeattack"
        }

        if (game.user.targets.size > 0) {
            // Push each targeted token onto the targets array
            game.user.targets.forEach((t) => {
                targets.push(t);
            })
        }

        // See if this is a weapon attack
        if (data.type === 'weapon') {
            const weapon = data.actor.getEmbeddedDocument('Item', data.itemId);
            damageType = weapon.data.data.damage.type;
            damageScore = weapon.data.data.damage.score;
            if (weapon.data.data.mods.damage !== 0) damageScore += weapon.data.data.mods.damage;
            if (weapon.data.data.mods.difficulty !== 0) miscMod += weapon.data.data.mods.difficulty;
            if (weapon.data.data.mods.attack !== 0) bonusmod += weapon.data.data.mods.attack;
            if (weapon.data.data.difficulty !== 0) miscMod += weapon.data.data.difficulty;

            if (data.subtype === 'meleeattack') {
                const strmod = {
                    "name": 'OD6S.STRENGTH_DAMAGE_BONUS',
                    "value": data.actor.data.data.strengthdamage.score
                }
                damageModifiers.push(strmod);
            }

            // Check for effect modifiers
            const stats = weapon.data.data.stats
            let found = false;
            if (typeof (stats.specialization) !== 'undefined' && stats.specialization !== '') {
                if (data.actor.items.filter(i => i.type === 'specialization' && i.name === stats.specialization)) {
                    bonusmod += (+this.getEffectMod('specialization', stats.specialization, data.actor));
                    found = true;
                }
            }

            if (!found && typeof (stats.skill) !== 'undefined' && stats.skill !== '') {
                if (data.actor.items.filter(i => i.type === 'skill' && i.name === stats.skill)) {
                    bonusmod += (+this.getEffectMod('skill', stats.skill, data.actor));
                }
            }

        }

        if (data.type === 'brawlattack') {
            damageType = 'p';
            damageScore = data.actor.data.data.strengthdamage.score;
        }

        if (data.type === 'action') {
            let skill = '';
            switch (data.subtype) {
                case 'attribute':
                    data.score = data.actor.data.data.attributes[data.attribute].score;
                    isVisible = !game.settings.get('od6s', 'hide-skill-cards');
                    break;
                case 'rangedattack':
                    // Use agi as base, skill dropdown in dialog
                    data.score = data.actor.data.data.attributes.agi.score;
                    isVisible = !game.settings.get('od6s', 'hide-combat-cards');
                    break;
                case 'meleeattack':
                    // Look for Melee Combat skill; use agi if not found.  Show skills/specs in dialog
                    skill = await data.actor.items.find(i => i.type === 'skill'
                        && i.name === game.i18n.localize('OD6S.MELEE_COMBAT'));
                    if (skill !== null && typeof (skill) !== 'undefined') {
                        data.score = skill.data.data.score +
                            data.actor.data.data.attributes[skill.data.data.attribute.toLowerCase()].score;
                    } else {
                        data.score = data.actor.data.data.attributes.agi.score;
                    }
                    isVisible = !game.settings.get('od6s', 'hide-combat-cards');
                    break;
                case 'brawlattack':
                    // Look for Brawl skill; use agi if not found.  Show skills/specs in dialog
                    skill = await data.actor.items.find(i => i.type === 'skill'
                        && i.name === game.i18n.localize('OD6S.BRAWL'));
                    if (skill !== null && typeof (skill) !== 'undefined') {
                        data.score = skill.data.data.score +
                            data.actor.data.data.attributes[skill.data.data.attribute.toLowerCase()].score;
                    } else {
                        const bAttr = game.settings.get('od6s', 'brawl_attribute')
                        data.score = data.actor.data.data.attributes[bAttr].score;
                    }
                    isVisible = !game.settings.get('od6s', 'hide-combat-cards');
                    break;
                case '':
                    if (data.name === game.i18n.localize('OD6S.ENERGY_RESISTANCE') ||
                        data.name === game.i18n.localize('OD6S.PHYSICAL_RESISTANCE')) {
                        data.type = 'resistance';
                    }
            }

        }

        const rollValues = od6sutilities.getDiceFromScore(data.score);

        const actionPenalty = ((+data.actor.itemTypes.action.length) > 0) ? (+data.actor.itemTypes.action.length) - 1 : 0;
        if (data.type === 'resistance') {
            woundPenalty = 0;
            isVisible = true;
        } else {
            woundPenalty = od6sutilities.getWoundPenalty(data.actor);
        }

        if (data.score < 3) {
            /* no score for this, we're done. */
            ui.notifications.warn(game.i18n.localize("OD6S.SCORE_TOO_LOW"));
            return;
        }

        if (data.type === 'skill' && data.name === 'Dodge') {
            data.subtype = 'Dodge';
        }

        if ((data.type === 'skill') || (data.type === 'specialization')) {
            isVisible = !game.settings.get('od6s', 'hide-skill-cards');
            // Get the attribute of the skill or spec
            attribute = data.actor.data.items.filter(i => i.id === data.itemId)[0].data.data.attribute.toLowerCase();
            if (typeof (attribute) === 'undefined') {
                attribute = null;
            }
        } else {
            attribute = null;
        }

        // See if there are any effects that should add a bonus to a skill roll
        if (data.type === 'skill') {
            const skillName = data.actor.data.items.filter(i => i.id === data.itemId)[0].name;
            bonusmod += (+this.getEffectMod('skill', skillName, data.actor));
        }

        if (data.type === 'specialization') {
            const specName = data.actor.data.items.filter(i => i.id === data.itemId)[0].name;
            bonusmod += (+this.getEffectMod('specialization', specName, data.actor));
        }

        let fatepointeffect = false;

        if (data.actor.getFlag('od6s', 'fatepointeffect')) {
            // Double all dice while fate point is active
            rollValues.dice = (+rollValues.dice) * 2;
            rollValues.pips = (+rollValues.pips) * 2;

            fatepointeffect = true;
        }

        if (data.subtype === 'parry' && data.type === 'weapon') {
            data.name = data.name + " " + game.i18n.localize('OD6S.PARRY');
        }

        if (data.type === 'skill' ||
            data.type === 'attribute' ||
            data.type === 'specialization' ||
            data.type === 'damage' || data.type === 'resistance') {
            isOpposable = true;
        }

        if (data.type === 'action' &&
            data.subtype === "meleeattack" &&
            data.name === game.i18n.localize('OD6S.ACTION_MELEE_ATTACK')) {
            // Treat as an improvised weapon.
            miscMod += 5;
            damageScore = (data.actor.data.data.strengthdamage.score *3) + 2;
        }

        if (data.subtype === 'rangedattack') {
            range = "OD6S.RANGE_SHORT_SHORT";
            bonusmod += (+data.actor.data.data.ranged.mod);
        }

        if (data.subtype === 'meleeattack') {
            bonusmod += (+data.actor.data.data.melee.mod);
        }

        if (data.subtype === 'brawlattack') {
            bonusmod += (+data.actor.data.data.brawl.mod);
        }

        if (data.subtype === 'dodge') {
            bonusmod += (+data.actor.data.data.dodge.mod);
        }

        if (data.subtype === 'parry') {
            bonusmod += (+data.actor.data.data.parry.mod);
        }

        if (data.subtype === 'block') {
            bonusmod += (+data.actor.data.data.block.mod);
        }

        bonusdice = od6sutilities.getDiceFromScore(bonusmod);

        this.rollData = {
            label: data.name,
            title: data.name,
            dice: rollValues.dice,
            pips: rollValues.pips,
            originaldice: rollValues.dice,
            originalpips: rollValues.pips,
            score: data.score,
            wilddie: game.settings.get('od6s','use_wild_die'),
            showWildDie: game.settings.get('od6s','use_wild_die'),
            fatepoint: Boolean(false),
            fatepointeffect: fatepointeffect,
            characterpoints: 0,
            cpcost: 0,
            cpcostcolor: "black",
            bonusdice: bonusdice.dice,
            bonuspips: bonusdice.pips,
            isvisible: isVisible,
            isknown: isKnown,
            type: data.type,
            subtype: data.subtype,
            attribute: attribute,
            actor: data.actor,
            actionpenalty: actionPenalty,
            woundpenalty: woundPenalty,
            otherpenalty: 0,
            multishot: false,
            shots: 1,
            fulldefense: false,
            itemid: data.itemId,
            targets: targets,
            target: targets[0],
            damagetype: damageType,
            damagescore: damageScore,
            damagemodifiers: damageModifiers,
            difficultylevel: 'OD6S.DIFFICULTY_EASY',
            isoppasable: isOpposable,
            difficulty: difficulty,
            modifiers: {
                range: range,
                attackoption: 'OD6S.ATTACK_STANDARD',
                calledshot: '',
                cover: '',
                coverlight: '',
                coversmoke: '',
                miscmod: miscMod
            }
        }
        let template = "systems/od6s/templates/roll.html";
        const html = await renderTemplate(template, this.rollData);

        new RollDialog(this, {
            title: game.i18n.localize("OD6S.ROLL") + "!",
            content: html,
            buttons: {
                submit: {
                    label: game.i18n.localize("OD6S.ROLL"),
                    callback: () => od6sroll.rollAction(this)
                }
            }
        }).render(true);
    }

    static async rollAction(caller) {
        const rollData = caller.rollData;
        const actor = caller.rollData.actor
        let rollString;
        let cpString;
        let targetName;
        let targetId;
        let difficulty = 10;
        let damageScore = rollData.damagescore;
        let damageType = rollData.damagetype;
        let baseDamage = 0;
        let doUpdate = false;
        let update = {};

        rollData.isknown = true;
        let rollMode = 'roll';
        // Using a fate point doubles the dice and pips of the roll
        if (rollData.fatepoint) {
            rollData.dice = (+rollData.originaldice * 2);
            rollData.pips = (+rollData.originalpips * 2);
        }

        // Subtract Penalties
        rollData.dice = (+rollData.dice) - (+rollData.actionpenalty) - (+rollData.woundpenalty) - (+rollData.otherpenalty);

        // Wild die explodes on a 6
        if (rollData.wilddie) {
            rollData.dice = (+rollData.dice) - 1;
            if (rollData.dice === 0) {
                rollString = '1d6x6[Wild]';
            } else if (rollData.dice <= 0) {
                rollString = '';
            } else {
                rollString = rollData.dice + "d6" + game.i18n.localize("OD6S.BASE_DIE_FLAVOR") + "+1dw" +
                    game.i18n.localize("OD6S.WILD_DIE_FLAVOR");
            }
        } else {
            if (rollData.dice <= 0) {
                rollString = ''
            } else {
                rollString = rollData.dice + "d6" + game.i18n.localize("OD6S.BASE_DIE_FLAVOR");
            }
        }

        if (rollData.pips > 0) {
            rollString += "+" + rollData.pips;
        }

        // Character point dice also explode on a 6
        if (rollData.characterpoints > 0) {
            cpString = "+" + rollData.characterpoints + "db"
                + game.i18n.localize("OD6S.CHARACTER_POINT_DIE_FLAVOR");
            rollString += cpString;
        }

        // Bonus pips are not calculated to add new dice, just a bonus
        if (rollData.bonusdice > 0) {
            rollString += "+" + rollData.bonusdice + "d6" + game.i18n.localize("OD6S.BONUS_DIE_FLAVOR");
        }
        if (rollData.bonuspips > 0) {
            rollString += "+" + rollData.bonuspips;
        }

        // Apply costs
        if ((rollData.characterpoints > 0) && (actor.data.data.characterpoints.value > 0)) {
            doUpdate = true;
            actor.data.data.characterpoints.value -= rollData.characterpoints;
        }

        if (rollData.fatepoint && (actor.data.data.fatepoints.value > 0)) {
            doUpdate = true;
            actor.data.data.fatepoints.value -= 1;
        }

        if (typeof (rollData.target) !== 'undefined') {
            targetName = rollData.target.name;
            targetId = rollData.target.id;
        }

        // Now, determine the target number to beat, if necessary
        if (rollData.difficulty) {
            difficulty = rollData.difficulty;
        } else {
            difficulty = this.getDifficulty(rollData);
        }
        const baseDifficulty = difficulty;
        const modifiers = this.applyDifficultyEffects(rollData);

        // Hide if "Unknown"
        if (rollData.difficultylevel === 'OD6S.DIFFICULTY_UNKNOWN') {
            rollData.isvisible = false;
            rollData.isknown = false;
        }

        if (game.settings.get('od6s', 'hide-skill-cards')) {
            rollData.isknown = false;
        }

        if (rollData.subtype === 'dodge' || rollData.subtype === 'parry' || rollData.subtype === 'block') {
            rollData.isknown = true;
            rollData.isvisible = true;
        }

        if (rollData.subtype === 'rangedattack' ||
            rollData.subtype === 'meleeattack' ||
            rollData.subtype === 'brawlattack') {
            modifiers.forEach(m => {
                difficulty = (+difficulty) + (+m.value);
            })

            if (difficulty < 0) difficulty = 0;

            if (rollData.subtype === 'brawlattack') {
                damageScore = actor.data.data.attributes.str.score;
                damageType = 'p';
            }

            baseDamage = damageScore;
            // Determine damage modifiers, if any
            const damageEffects = this.applyDamageEffects(rollData);
            rollData.damagemodifiers = rollData.damagemodifiers.concat(damageEffects);
            if (typeof (rollData.damagemodifiers) !== 'undefined' && rollData.damagemodifiers.length) {
                rollData.damagemodifiers.forEach(d => {
                    damageScore = (+damageScore) + (d.value);
                })
            }
        }

        let flags = {
            "actorId": rollData.actor.id,
            "targetName": targetName,
            "targetId": targetId,
            "baseDifficulty": baseDifficulty,
            "difficulty": difficulty,
            "baseDamage": baseDamage,
            "damageScore": damageScore,
            "damageModifiers": rollData.damagemodifiers,
            "damageType": damageType,
            "damageTypeName": OD6S.damageTypes[damageType],
            "range": rollData.range,
            "type": rollData.type,
            "subtype": rollData.subtype ? rollData.subtype : '',
            "attackOption": rollData.attackoption,
            "multiShot": rollData.multishot,
            "modifiers": modifiers,
            "isEditable": true,
            "editing": false,
            "isVisible": rollData.isvisible,
            "isKnown": rollData.isknown,
            "isOpposable": rollData.isoppasable,
            "wild": false,
            "wildHandled": false,
            "wildResult": "OD6S.WILD_RESULT_ONE"
        }

        // Let's roll!
        if (rollString === '') {
            ui.notifications.warn(game.i18n.localize('OD6S.ZERO_DICE'));
            return;
        }

        let roll = await new Roll(rollString).evaluate({"async": true});
        let label = rollData.label ? `${game.i18n.localize('OD6S.ROLLING')} ${rollData.label}` : '';

        roll.terms.find(d => d.flavor === "Wild").total === 1 ? flags.wild = true : flags.wild = false;
        flags.success = roll.total >= difficulty;
        flags.total = roll.total;

        if (game.user.isGM && game.settings.get('od6s', 'hide-gm-rolls')) rollMode = CONST.DICE_ROLL_MODES.PRIVATE;
        await roll.toMessage({
                speaker: ChatMessage.getSpeaker({actor: actor}),
                flavor: label,
                flags: {od6s: flags}
            },
            {rollMode: rollMode}
        );

        if (rollData.subtype === 'dodge' || rollData.subtype === 'parry' || rollData.subtype === 'block') {
            doUpdate = true;
            if (rollData.fulldefense) {
                actor.data.data[rollData.subtype].score = (+roll.total + 10);
            } else {
                actor.data.data[rollData.subtype].score = (+roll.total);
            }
        }

        if (doUpdate) {
            update.data = {};
            update.data.fatepoints = actor.data.data.fatepoints;
            update.data.characterpoints = actor.data.data.characterpoints;
            update.data.dodge = {};
            update.data.dodge.score = actor.data.data.dodge.score;
            update.data.parry = {};
            update.data.parry.score = actor.data.data.parry.score;
            update.data.block = {};
            update.data.block.score = actor.data.data.block.score;
            await actor.update(update);
        }

        if (rollData.fatepoint) {
            await actor.setFlag('od6s', 'fatepointeffect', true)
        }
        await actor.render();
    }

    /**
     * Get the base difficulty for a roll
     * @param rollData
     * @returns {number|*}
     */
    static getDifficulty(rollData) {
        const target = typeof (rollData.target) !== 'undefined';
        // If the roll is an attack and has a target, get the appropriate defense value from the target, if any
        switch (rollData.subtype) {
            case 'rangedattack':
                if (target && (+rollData.target.actor.data.data.dodge.score) > 0) {
                    return (+rollData.target.actor.data.data.dodge.score);
                } else {
                    return 10;
                }
            case 'meleeattack':
            case 'brawlattack':
                if (target) {
                    const targetData = rollData.target.actor.data.data;
                    if (targetData.block.score === 0 && targetData.dodge.score === 0 && targetData.parry.score === 0) {
                        return 10;
                    } else {
                        // Look at dodge, parry, and block take the highest
                        if (targetData.dodge.score >= targetData.parry.score && targetData.dodge.score >= targetData.block.score) {
                            return targetData.dodge.score;
                        } else if (targetData.parry.score >= targetData.dodge.score && targetData.parry.score >= targetData.block.score) {
                            return targetData.parry.score;
                        } else {
                            return targetData.block.score;
                        }
                    }

                } else {
                    return 10;
                }

            default:
        }

        switch (rollData.type) {
            case 'resistance':
            case 'dodge':
            case 'parry':
            case 'block':
                return 0;

            default:
                return OD6S.difficulty[rollData.difficultylevel].max;
        }
    }

    /**
     * Assemble difficulty modifiers based on roll data and target conditions
     * @param rollData
     * @returns {[]}
     * @constructor
     */
    static applyDifficultyEffects(rollData) {
        const mods = rollData.modifiers;
        let difficultyModifiers = [];
        let modifiers = [];

        // First, handle modifiers passed to the roll
        if (rollData.subtype === 'rangedattack') {
            if (OD6S.ranges[mods.range].difficulty) {
                modifiers.push({
                    "name": game.i18n.localize(OD6S.ranges[mods.range].name),
                    "value": OD6S.ranges[mods.range].difficulty
                })
            }

            if (OD6S.rangedAttackOptions[mods.attackoption].attack) {
                let value;
                if (OD6S.rangedAttackOptions[mods.attackoption].multi) {
                    value = OD6S.rangedAttackOptions[mods.attackoption].attack * (rollData.shots - 1);
                } else {
                    value = OD6S.rangedAttackOptions[mods.attackoption].attack;
                }

                modifiers.push({
                    "name": game.i18n.localize(mods.attackoption),
                    "value": value
                })
            }
        }

        if (rollData.subtype === 'meleeattack') {
            if (OD6S.ranges[mods.range].difficulty) {
                modifiers.push({
                    "name": game.i18n.localize(OD6S.ranges[mods.range].name),
                    "value": OD6S.ranges[mods.range].difficulty
                })
            }

            if (OD6S.meleeAttackOptions[mods.attackoption].attack) {
                modifiers.push({
                    "name": game.i18n.localize(mods.attackoption),
                    "value": OD6S.meleeAttackOptions[mods.attackoption].attack
                })
            }
        }

        if (rollData.subtype === 'brawlattack') {
            if (OD6S.ranges[mods.range].difficulty) {
                modifiers.push({
                    "name": game.i18n.localize(OD6S.ranges[mods.range].name),
                    "value": OD6S.ranges[mods.range].difficulty
                })
            }

            if (OD6S.brawlAttackOptions[mods.attackoption].attack) {
                modifiers.push({
                    "name": game.i18n.localize(mods.attackoption),
                    "value": OD6S.brawlAttackOptions[mods.attackoption].attack
                })
            }
        }

        if (mods.cover !== '' && OD6S.cover["OD6S.COVER"][mods.cover].modifier !== 0) {
            modifiers.push({
                "name": game.i18n.localize(mods.cover),
                "value": OD6S.cover["OD6S.COVER"][mods.cover].modifier
            })
        }

        if (mods.coverlight !== '' && OD6S.cover["OD6S.COVER_LIGHT"][mods.coverlight].modifier !== 0) {
            modifiers.push({
                "name": game.i18n.localize(mods.coverlight),
                "value": OD6S.cover["OD6S.COVER_LIGHT"][mods.coverlight].modifier
            })
        }

        if (mods.coversmoke !== '' && OD6S.cover["OD6S.COVER_SMOKE"][mods.coversmoke].modifier !== 0) {
            modifiers.push({
                "name": game.i18n.localize(mods.coversmoke),
                "value": OD6S.cover["OD6S.COVER_SMOKE"][mods.coversmoke].modifier
            })
        }

        if (mods.calledshot !== '' && OD6S.calledShot[mods.calledshot].modifier !== 0) {
            modifiers.push({
                "name": game.i18n.localize(mods.calledshot),
                "value": OD6S.calledShot[mods.calledshot].modifier
            })
        }

        if (mods.miscmod !== 0 && mods.miscmod !== '') {
            modifiers.push({
                "name": game.i18n.localize("OD6S.MISC"),
                "value": mods.miscmod
            })
        }

        modifiers.forEach(m => {
            difficultyModifiers.push(m);
        })
        return difficultyModifiers;
    }

    static applyDamageEffects(rollData) {
        const mods = rollData.modifiers;
        let modifiers = [];

        if (rollData.subtype === 'rangedattack') {
            if (OD6S.rangedAttackOptions[mods.attackoption].damage) {
                let value;
                if (OD6S.rangedAttackOptions[mods.attackoption].multi) {
                    value = OD6S.rangedAttackOptions[mods.attackoption].damage * (rollData.shots - 1);
                } else {
                    value = OD6S.rangedAttackOptions[mods.attackoption].damage;
                }

                modifiers.push({
                    "name": mods.attackoption,
                    "value": value
                })
            }
        }

        if (rollData.subtype === 'meleeattack') {
            if (OD6S.meleeAttackOptions[mods.attackoption].damage) {
                modifiers.push({
                    "name": mods.attackoption,
                    "value": OD6S.meleeAttackOptions[mods.attackoption].damage
                })
            }
        }

        if (rollData.subtype === 'brawlattack') {
            if (OD6S.brawlAttackOptions[mods.attackoption].damage) {
                modifiers.push({
                    "name": mods.attackoption,
                    "value": OD6S.brawlAttackOptions[mods.attackoption].damage
                })
            }
        }
        return modifiers;
    }

    static getEffectMod(type, name, actor) {
        // See if there are any effects that should add a bonus to a skill roll
        if (type === 'skill') {
            if (typeof (actor.data.data.customeffects.skills[name]) !== 'undefined') {
                return actor.data.data.customeffects.skills[name];
            }
        }

        if (type === 'specialization') {
            if (typeof (actor.data.data.customeffects.specializations[name]) !== 'undefined') {
                return actor.data.data.customeffects.specializations[name];
            }

            // See if the base skill has any modifiers
            const spec = actor.data.items.filter(i => i.type === type && i.name === name)[0];
            if (typeof (spec) !== 'undefined') {
                if (typeof (actor.data.data.customeffects.skills[spec.data.data.skill]) !== 'undefined') {
                    return actor.data.data.customeffects.skills[spec.data.data.skill];
                }
            }
        }

        return 0;
    }
}
