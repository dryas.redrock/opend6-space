import {od6sroll} from "../od6sroll.js";
import {od6sInitRoll} from "../od6sroll.js";
import {od6sadvance} from "./advance.js";
import {od6sspecialize} from "./specialize.js";
import {od6sattributeedit} from "./attribute-edit.js";
import {od6sutilities} from "../utilities.js";
import OD6S from "../config/config-od6s.js";

//import OD6S from "../config/config-od6s.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class OD6SActorSheet extends ActorSheet {

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["od6s", "sheet", "actor"],
            width: 915,
            height: 800,
            tabs: [{navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "attributes"}]
        });
    }

    /** @override */
    get template() {
        return `systems/od6s/templates/actor/${this.actor.data.type}/${this.actor.data.type}-sheet.html`;
    }

    /* -------------------------------------------- */

    /** @override */
    getData() {
        const data = super.getData();
        data.dtypes = ["String", "Number", "Boolean"];

        // Prepare items.
        this._prepareCharacterItems(data);

        // Set common flags
        if (typeof (this.actor.getFlag('od6s', 'fatepointeffect')) === 'undefined') {
            this.actor.setFlag('od6s', 'fatepointeffect', false);
        }
        if (typeof (this.actor.getFlag('od6s', 'hasTakenTurn')) === 'undefined') {
            this.actor.setFlag('od6s', 'hasTakenTurn', false);
        }

        data.data.items = data.items;
        data.data.actor = data.actor;
        return data.data;
    }

    /**
     * Organize and classify Items for Character sheets.
     *
     * @param {Object} sheetData The actor to prepare.
     *
     * @return {undefined}
     */
    _prepareCharacterItems(sheetData) {
        const actorData = sheetData.actor;
        // Initialize containers.
        const gear = [];
        const features = [];
        const skills = [];
        const specializations = [];
        const weapons = [];
        const armor = [];
        const advantages = [];
        const disadvantages = [];
        const specialabilities = [];
        const cybernetics = [];
        const manifestations = [];
        const actions = [];

        // Iterate through items, allocating to containers
        // let totalWeight = 0;
        for (let i of sheetData.items) {
            i.id = i._id;
            i.img = i.img || CONST.DEFAULT_TOKEN;
            // Append to gear.
            if (i.type === 'gear') {
                gear.push(i);
            }

            // Append to skills.
            else if (i.type === 'skill') {
                i.data.score = (+i.data.score) + (+actorData.data.data.attributes[i.data.attribute.toLowerCase()].score);
                skills.push(i);
            }

            // Append to specializations
            else if (i.type === 'specialization') {
                i.data.score = (+i.data.score) +
                    (+actorData.data.data.attributes[i.data.attribute.toLowerCase()].score);
                specializations.push(i);
            }
            // Append to weapons
            else if (i.type === 'weapon') {
                weapons.push(i);
            } else if (i.type === 'armor') {
                armor.push(i);
            } else if (i.type === 'advantage') {
                advantages.push(i);
            } else if (i.type === 'disadvantage') {
                disadvantages.push(i);
            } else if (i.type === 'specialability') {
                specialabilities.push(i);
            } else if (i.type === "cybernetic") {
                cybernetics.push(i);
            } else if (i.type === "manifestation") {
                manifestations.push(i);
            } else if (i.type === "action") {
                actions.push(i);
            }
        }

        // Assign and return
        actorData.gear = gear;
        actorData.features = features;
        actorData.skills = skills;
        actorData.specializations = specializations;
        actorData.weapons = weapons;
        actorData.armor = armor;
        actorData.advantages = advantages;
        actorData.disadvantages = disadvantages;
        actorData.specialabilities = specialabilities;
        actorData.cybernetics = cybernetics;
        actorData.manifestations = manifestations;
        actorData.actions = actions;
    }

    /* -------------------------------------------- */

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);

        // Everything below here is only needed if the sheet is editable
        if (!this.options.editable) return;

        // Free edit attribute
        const attributeEditDialog = new od6sattributeedit();
        html.find('.attribute-edit').click(attributeEditDialog._onAttributeEdit.bind(this));

        // Add Inventory Item
        html.find('.item-create').click(this._onItemCreate.bind(this));

        // Update Inventory Item
        html.find('.item-edit').click(ev => {
            const li = $(ev.currentTarget).parents(".item");
            const item = this.actor.items.get(li.data("itemId"));
            item.sheet.render(true);
        });

        // Delete Inventory Item
        html.find('.item-delete').click(async ev => {
            ev.preventDefault();
            // If this is a skill, deny if there are existing specializations.
            if (ev.currentTarget.dataset.type === "skill") {
                for (let i in this.actor.data.items) {
                    if (this.actor.data.items[i].type === "specialization") {
                        if (this.actor.data.items[i].data.skill === ev.currentTarget.dataset.itemId) {
                            ui.notifications.error(game.i18n.localize("OD6S.ERR_SKILL_HAS_SPEC"));
                            return;
                        }
                    }
                }
            }
            if (ev.currentTarget.dataset.confirm !== "false") {
                const confirmText = "<p>" + game.i18n.localize("OD6S.DELETE_CONFIRM") + "</p>";
                await Dialog.prompt({
                    title: game.i18n.localize("OD6S.DELETE"),
                    content: confirmText,
                    callback: async () => {
                        const li = $(ev.currentTarget).parents(".item");
                        await this.actor.deleteEmbeddedDocuments('Item', [li.data("itemId")]);
                        await this.render(false);
                    }
                })
            } else {
                const li = $(ev.currentTarget).parents(".item");
                await this.actor.deleteEmbeddedDocuments('Item', [ev.currentTarget.dataset.itemId]);
                await this.render(false);
            }

        });

        // Rollable abilities.
        let rollDialog = new (od6sroll);
        html.find('.rolldialog').click(rollDialog._onRollEvent.bind(this));
        html.find('.initrolldialog').click(od6sInitRoll._onInitRollDialog.bind(this));
        html.find('.actionroll').click(rollDialog._onRollItem.bind(this));

        // Attribute/skill advances
        let advanceDialog = new (od6sadvance);
        html.find('.advancedialog').click(advanceDialog._onAdvance.bind(this));

        // Attribute context menu
        html.find('.attributedialog').contextmenu(() => {
        })

        // Skill context menu
        html.find('.skilldialog').contextmenu(() => {
        })

        // Skill specialization
        let specializeDialog = new (od6sspecialize);
        html.find('.specializedialog').click(specializeDialog._onSpecialize.bind(this));

        // Reset template/actor
        html.find('.reset-template').click(() => {
            const confirmText = "<p>" + game.i18n.localize("OD6S.CONFIRM_TEMPLATE_CLEAR") + "</p>";
            Dialog.prompt({
                title: game.i18n.localize("OD6S.CLEAR_TEMPLATE"),
                content: confirmText,
                callback: () => {
                    return this._onClearCharacterTemplate();
                }
            })
        });

        // Add/remove actions
        html.find('.addaction').click(() => {
            this._onActionAdd();
        })

        html.find('.combat-action').contextmenu((ev) => {
            this._onAvailableActionAdd(ev);
        })

        // Roll available action
        html.find('.combat-action').click(async (ev) => {
            await this._rollAvailableAction(ev);
        })

        // Edit active effect
        html.find('.edit-effect').click(async (ev) => {
            await this._editEffect(ev);
        })

        // Fate point in effect checkbox
        html.find('.fatepointeffect').change(async () => {
            // Don't allow if actor has 0 points
            if (this.actor.data.data.fatepoints.value < 1) {
                await this.actor.setFlag('od6s', 'fatepointeffect', false)
                await this.render();
                return;
            }

            let inEffect = this.actor.getFlag('od6s', 'fatepointeffect');
            await this.actor.setFlag('od6s', 'fatepointeffect', !inEffect);
            inEffect = this.actor.getFlag('od6s', 'fatepointeffect');
            if (inEffect) {
                const update = {};
                update.data = {};
                update.data.fatepoints = {};
                update.id = this.actor.id;
                update.data.fatepoints.value = this.actor.data.data.fatepoints.value -= 1;
                await this.actor.update(update, {diff: true});
            }
        })

        // Drag events
        if (this.actor.isOwner) {
            let handler = ev => this._onDragStart(ev);
            // Items
            html.find('li.item').each((i, li) => {
                if (li.classList.contains("inventory-header")) return;
                li.setAttribute("draggable", true);
                li.addEventListener("dragstart", handler, false);
            });
            // Combat Actions
            html.find('li.availableaction').each((i, li) => {
                li.setAttribute("draggable", true);
                li.addEventListener("dragstart", this._dragAvailableCombatAction, false);
            })
        }
    }

    /**
     * Adds a 'generic' action to the action list
     * @returns {Promise<void>}
     * @private
     */
    async _onActionAdd() {
        const data = {
            name: "Other miscellaneous action"
        }
        await this._createAction(data);
        await this.render();
    }

    /**
     * Add an action via a right-click
     * @returns {Promise<void>}
     * @private
     */
    async _onAvailableActionAdd(event) {
        const data = {
            name: event.currentTarget.dataset.name,
            type: "availableaction",
            subtype: event.currentTarget.dataset.type,
            itemId: event.currentTarget.dataset.id,
            rollable: event.currentTarget.dataset.rollable
        }
        await this._createAction(data);
    }

    /**
     * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
     * @param {Event} event   The originating click event
     * @private
     */
    _onItemCreate(event) {
        event.preventDefault();
        const header = event.currentTarget;
        // Get the type of item to create.
        const type = header.dataset.type;
        // Grab any data associated with this control.
        const data = duplicate(header.dataset);
        // Initialize a default name.
        const name = `New ${type.capitalize()}`;
        // Prepare the item object.
        const itemData = {
            name: name,
            type: type,
            data: data
        };
        // Remove the type from the dataset since it's in the itemData.type prop.
        delete itemData.data["type"];

        // Finally, create the item!
        return this.actor.createEmbeddedDocuments("Item", [itemData]);
    }

    /**
     * Add a character template to an actor
     * @param event
     * @param item
     * @param data
     * @returns {Promise<boolean|*>}
     * @private
     */
    async _onDropCharacterTemplate(event, item, data) {
        if (!this.actor.isOwner) return false;
        if (this.actor.data.type !== 'character') return false;
        const actor = this.actor;
        const itemData = item.data.data;

        // Check if a template has already been assigned to this actor
        if (this.actor.data.items.find(E => E.type === 'character-template')) {
            ui.notifications.error(game.i18n.localize("OD6S.ERROR_TEMPLATE_ALREADY_ASSIGNED"));
            return false;
        } else {
            // Set the actor's data to be equal to the data found in the template
            actor.data.data.chartype.content = item.name;
            actor.data.data.species.content = itemData.species;
            actor.data.data.fatepoints.value = itemData.fp;
            actor.data.data.characterpoints.value = itemData.cp;
            actor.data.data.credits.value = itemData.credits;
            actor.data.data.funds.score = itemData.funds;
            actor.data.data.move.value = itemData.move;
            actor.data.data.background.content = itemData.description;
            actor.data.data.metaphysicsextranormal.value = itemData.me;

            for (const attribute in itemData.attributes) {
                actor.data.data.attributes[attribute].base += itemData.attributes[attribute];
            }
            const update = {};
            update.data = this.actor.data.data;
            update.id = this.actor.id;
            await this.actor.update(update, {diff: true});

            // Loop through template items and add to actor from world, then compendia
            // Filter out items if config is set to do so
            let templateItems = [];
            for (let i of itemData.items) {
                let templateItem = await od6sutilities._getItemFromWorld(i.name);
                if (typeof (templateItem) === 'undefined') {
                    // Check compendia
                    templateItem = await od6sutilities._getItemFromCompendium(i.name);
                    if (typeof (templateItem) === 'undefined') {
                        break;
                    }
                }
                if ((i.type === 'advantage' || i.type === 'disadvantage') &&
                    game.settings.get('od6s','hide_advantages_disadvantages')) break;
                if (typeof i.description !== 'undefined' && i.description !== '' && i.description !== null) {
                    templateItem.data.data.description = i.description;
                }
                templateItems.push(templateItem.data);
            }
            templateItems.push(item.data);
            if (templateItems.length) {
                await this.actor.createEmbeddedDocuments('Item', templateItems);
            }
        }
    }

    /**
     * Override
     */
    async _onDrop(event) {
        event.preventDefault();
        // Try to extract the data
        let data;
        try {
            data = JSON.parse(event.dataTransfer.getData('text/plain'));
        } catch (err) {
            return false;
        }
        const actor = this.actor;
        // Handle the drop with a Hooked function
        const allowed = Hooks.call("dropActorSheetData", actor, this, data);
        if (allowed === false) return;

        // Handle different data types
        switch (data.type) {
            case "ActiveEffect":
                return this._onDropActiveEffect(event, data);
            case "Actor":
                return this._onDropActor(event, data);
            case "Item":
                const item = await Item.fromDropData(data);
                switch (item.data.type) {
                    case "character-template":
                        return this._onDropCharacterTemplate(event, item, data);
                    case "skill":
                        if (typeof (item.data.data.attribute) === 'undefined' || item.data.data.attribute === '') {
                            ui.notifications.error(game.i18n.localize('OD6S.MISSING_ATTRIBUTE'))
                            return;
                        } else {
                            return this._onDropItem(event, data);
                        }
                    case "specialization":
                        if (typeof (item.data.data.attribute) === 'undefined' || item.data.data.attribute === '') {
                            ui.notifications.error(game.i18n.localize('OD6S.MISSING_ATTRIBUTE'))
                            return;
                        } else if (typeof (item.data.data.attribute) === 'undefined' || item.data.data.skill === '') {
                            ui.notifications.error(game.i18n.localize('OD6S.MISSING_SKILL'))
                            return;
                        } else if (!(actor.items.find(i => i.type === 'specialization' && i.name === item.data.name))) {
                            ui.notifications.warn(game.i18n.localize('OD6S.DOES_NOT_POSSESS_SKILL'));
                            return;
                        } else {
                            return this._onDropItem(event, data);
                        }
                    default:
                        return this._onDropItem(event, data);
                }
            case "Folder":
                return this._onDropFolder(event, data);
            case "availableaction":
                return await this._createAction(data);
        }
    }

    /**
     * Creates an action
     * @param data
     * @returns {Promise<Object>}
     * @private
     */
    async _createAction(data) {
        // Localize system actions
        if (data.name.startsWith('OD6S.')) {
            data.name = game.i18n.localize(data.name);
        }

        // Only one dodge/parry/block needed per turn
        if (data.subtype === 'dodge' || data.subtype === 'parry' || data.subtype === 'block') {
            if (this.actor.itemTypes.action.find(i => i.data.data.subtype === data.subtype)) {
                ui.notifications.warn(game.i18n.localize('OD6S.ACTION_ONLY_ONE'));
                return;
            }
        }

        const action = {
            name: data.name,
            type: 'action',
            data: {
                type: data.type,
                subtype: data.subtype,
                rollable: data.rollable,
                itemId: data.itemId, // Used for item rolls
            }
        }
        return await this.actor.createEmbeddedDocuments('Item', [action]);
    }

    /**
     * Clear the template of an actor
     * @returns {Promise<boolean>}
     * @private
     */
    async _onClearCharacterTemplate() {
        // Find the template
        const item = this.actor.data.items.find(E => E.type === 'character-template');
        if (item) {
            const itemData = item.data;
            // Clear template stuff from the actor
            for (const attribute in itemData.data.attributes) {
                this.actor.data.data.attributes[attribute].base -= itemData.data.attributes[attribute];
                if (this.actor.data.data.attributes[attribute].base < 0) this.actor.data.data.attributes[attribute].base = 0;
            }

            this.actor.data.data.chartype.content = "";
            this.actor.data.data.species.content = "";
            this.actor.data.data.fatepoints.value -= itemData.fp;
            this.actor.data.data.characterpoints.value -= itemData.cp;
            this.actor.data.data.credits.value -= itemData.credits;
            this.actor.data.data.funds.score -= itemData.funds;
            this.actor.data.data.background.content = "";
            this.actor.data.data.metaphysicsextranormal.value = false;
            this.actor.data.data.move.value = 10;
            const update = {};
            update.id = this.actor.id;
            update.data = this.actor.data.data;
            await this.actor.update(update, {diff: true});

            if (itemData.data.items !== null && typeof (itemData.data.items !== 'undefined')) {
                for (let templateItem of itemData.data.items) {
                    let actorItem = this.actor.data.items.find(I => I.name === templateItem.name);
                    if (typeof (actorItem) !== 'undefined') {
                        await this.actor.deleteEmbeddedDocuments('Item', [actorItem.id]);
                    }
                }
            }

            await this.actor.deleteEmbeddedDocuments('Item', [item.id]);
            await this.render();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Enrich draggable combat actions
     * @param event
     * @returns {Promise<void>}
     * @private
     */
    async _dragAvailableCombatAction(event) {
        const data = event.target.children[0].dataset;
        const transferData = {
            name: data.name,
            type: "availableaction",
            subtype: data.type,
            itemId: data.id,
            rollable: data.rollable
        }
        return event.dataTransfer.setData("text/plain", JSON.stringify(transferData));
    }

    /**
     * Roll an available action
     * @param ev
     * @returns {Promise<*>}
     * @private
     */
    async _rollAvailableAction(ev) {
        let rollData = {};
        const data = ev.currentTarget.dataset;
        let name = game.i18n.localize(data.name);

        if (data.rollable !== "true") return;
        if (data.id !== '') {
            // Item, find the item and hand the roll off
            const item = this.actor.items.find(i => i.id === data.id);
            if (item !== null && typeof (item) !== 'undefined') {
                return await item.roll(data.type === 'parry');
            }
        }

        if (data.type === 'dodge' || data.type === 'parry' || data.type === 'block') {
            // Get the appropriate skill or attribute
            switch (data.type) {
                case 'dodge':
                    name = OD6S.actions.dodge.skill;
                    break;
                case 'parry':
                    name = OD6S.actions.parry.skill;
                    break;
                case 'block':
                    name = OD6S.actions.block.skill;
                    break;
            }
            name = game.i18n.localize(name);
        }

        // Create roll data
        if (data.type === 'attribute') {
            name = data.name;
            rollData.attribute = data.id;
        } else {
            let skill = this.actor.items.find(i => i.type === 'skill' && i.name === name);
            if (skill !== null && typeof (skill) !== 'undefined') {
                rollData.score = (+skill.data.data.score) +
                    (+this.actor.data.data.attributes[skill.data.data.attribute.toLowerCase()].score);
            } else {
                // Search compendia for the skill and use the attribute
                skill = await od6sutilities._getItemFromCompendium(name);
                if (skill !== null && typeof (skill) !== 'undefined') {
                    rollData.score = (+skill.data.data.score) +
                        (+this.actor.data.data.attributes[skill.data.data.attribute.toLowerCase()].score);
                } else {
                    skill = await od6sutilities._getItemFromWorld(name);
                    if (skill !== null && typeof (skill) !== 'undefined') {
                        rollData.score = (+this.actor.data.data.attributes[skill.data.data.attribute.toLowerCase()].score);
                    } else {
                        // Cannot find, use defaults for the type
                        for (let a in OD6S.actions) {
                            if (OD6S.actions[a].type === ev.currentTarget.dataset.type) {
                                rollData.score = (+this.actor.data.data.attributes[OD6S.actions[a].base].score);
                                break;
                            }
                        }
                    }
                }
            }
        }

        rollData.name = name;
        rollData.type = 'action';
        rollData.actor = this.actor;
        rollData.subtype = data.type;

        await od6sroll._onRollDialog(rollData);
    }

    async _editEffect(ev) {
        //const effect = this.document.getEmbeddedDocument("ActiveEffect", ev.currentTarget.dataset.effectId);
        const effect = this.actor.effects.find(e => e.id === ev.currentTarget.dataset.effectId);
        const sheet = new ActiveEffectConfig(effect);
        sheet.render(true);
    }

}

export default OD6SActorSheet;
