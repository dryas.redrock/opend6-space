import {od6sutilities} from "../utilities.js";
import {od6sroll} from "../od6sroll.js";
import OD6S from "../config/config-od6s.js";

/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class OD6SActor extends Actor {

    /**
     * Augment the basic actor data with additional dynamic data.
     */
    async _preCreate(data, options, user) {
        if (this.type === 'character') {
            this.data.token.update({vision: true, actorLink: true, dispsition: 1});
        }
    }

    prepareData() {
        super.prepareData();

        const actorData = this.data;
        const data = actorData.data;
        const flags = actorData.flags;

        // Make separate methods for each Actor type (character, npc, etc.) to keep
        // things organized.
        // if (actorData.type === 'character') this._prepareCharacterData(actorData);
    }

    prepareDerivedData() {
        const actorData = this.data;

        for (let a in actorData.data.attributes) {
            actorData.data.attributes[a].score = actorData.data.attributes[a].base + actorData.data.attributes[a].mod;
        }

        od6sutilities.setStrengthDamageBonus(actorData);
        od6sutilities.setInitiative(actorData);
        if (this.type !== 'vehicle') {
            actorData.data.pr.score = this.setResistance('pr')
            actorData.data.er.score = this.setResistance('er')
        }

    }

    async applyActiveEffects() {
        const overrides = {};

        // Organize non-disabled effects by their application priority
        const changes = this.effects.reduce((changes, e) => {
            if (e.data.disabled) return changes;
            return changes.concat(e.data.changes.map(c => {
                c = foundry.utils.duplicate(c);
                c.effect = e;
                c.priority = c.priority ?? (c.mode * 10);
                return c;
            }));
        }, []);
        changes.sort((a, b) => a.priority - b.priority);
        // Apply all changes
        for (let change of changes) {
            // Check if it is an item change
            if (change.key.startsWith("data.items")) {
                const t = change.key.split('.');
                if (t[2] === 'skills' || t[2] === 'skill' || t[2] === 'specializations' || t[2] === 'specialization') {
                    const item = this.items.find(i => i.name === t[3]);
                    if (typeof (item) !== 'undefined') {
                        const update = {};
                        update.id = item.id;
                        update.data = {};
                        update.data[t[4]] = {};
                        if (change.mode === 2) {
                            update.data[t[4]] = (+change.value);
                            await item.update(update);
                        }
                    }
                } else if (t[2] === "weapon" || t[2] === "weapons") {
                    const item = this.items.find(i => i.name === t[3]);
                    if (typeof (item) !== 'undefined') {
                        const item = this.items.find(i => i.name === t[3]);
                        const update = {};
                        update.id = item.id;
                        update.data = {};
                        if (t[4] === 'mods') {
                            update.data[t[4]] = {};
                            if (change.mode === 2) {
                                update.data[t[4]][t[5]] = (+change.value);
                                await item.update(update);
                            }
                        } else {
                            if (change.mode === 2) {
                                update.data[t[4]] = (+change.value);
                                item.update(update);
                            }
                        }
                    }
                }
            } else {
                const result = change.effect.apply(this, change);
                if (result !== null) overrides[change.key] = result;
            }
        }

        // Expand the set of final overrides
        this.overrides = foundry.utils.expandObject(overrides);
    }

    async rollAttribute(attribute) {
        const data = {
            "actor": this,
            "itemId": "",
            "name": OD6S.attributes[attribute].name,
            "score": this.data.data.attributes[attribute].score,
            "type": "attribute"
        }
        await od6sroll._onRollDialog(data);
    }

    async rollAction(actionId) {
        let actor = this;
        let itemId = '';
        let name = '';
        let score = 0;
        let type = '';

        switch (actionId) {
            case "rangedattack":
            case "meleeattack":
            case "brawlattack":
            case "dodge":
            case "parry":
            case "block":
                type = actionId;
                let result = '';
                for (let k in OD6S.actions) {
                    if (OD6S.actions[k].rollable && OD6S.actions[k].type === type) {
                        name = game.i18n.localize(OD6S.actions[k].name);
                        if (OD6S.actions[k].skill) {
                            const skill = actor.items.find(i => i.name === name);
                            if (skill !== null) {
                                score = (+skill.data.data.score) +
                                    (+this.data.data.attributes[skill.data.data.attribute.toLowerCase()].score);
                            } else {
                                score = actor.data.data.attributes[OD6S.actions[k].base].score;
                            }
                        } else {
                            score = actor.data.data.attributes[OD6S.actions[k].base].score;
                        }
                    }
                }
                break;
            case "er":
                name = game.i18n.localize(actor.data.data.er.label)
                score = actor.data.data.er.score;
                break;

            case "pr":
                name = game.i18n.localize(actor.data.data.pr.label)
                score = actor.data.data.pr.score;
                break;

            default:
                const item = actor.items.find(i => i.id === actionId);
                if (item !== null) {
                    return await item.roll()
                }
        }

        const data = {
            "actor": this,
            "itemId": itemId,
            "name": name,
            "score": score,
            "type": "action",
            "subtype": type
        }
        await od6sroll._onRollDialog(data);
    }

    async applyWounds() {
        // Adjust actor flags for the character's wound level
        if (this.data.type === 'character') {
            // Use the custom system wounds

        } else {
            // Peon, use standard wounds
        }
    }

    setResistance(type) {
        // Get PR from armor and add it to strength
        if (this.type === 'vehicle') return 0;
        let dr = this.data.data.attributes.str.score + this.data.data[type].mod;
        if (this.itemTypes.armor) {
            this.itemTypes.armor.forEach((value, index, array) => {
                dr += value.data.data[type];
            })
        }
        return dr;
    }

    async useCharacterPointOnRoll(message) {
        // Roll 1d6x6 and deduct a character point from the actor
        const actor = game.actors.get(message.data.speaker.actor);
        const rollString = "1d6x6[CP]";
        let roll = await new Roll(rollString).evaluate({"async": true});
        if (game.modules.get('dice-so-nice') && game.modules.get('dice-so-nice').active) {
            game.dice3d.showForRoll(roll, game.user, true, false, false);
        }

        const update = {};
        update.id = actor.id;
        update.data = {};
        update.data.characterpoints = {};
        update.data.characterpoints.value = actor.data.data.characterpoints.value -= 1;

        switch (message.getFlag('od6s', 'subtype')) {
            case "dodge":
                update.data.dodge = {};
                update.data.dodge.score = actor.data.data.dodge.score + roll.total;
                break;
            case "parry":
                update.data.parry = {};
                update.data.parry.score = actor.data.data.parry.score + roll.total;
                break;
            case "block":
                update.data.block = {};
                update.data.block.score = actor.data.data.block.score + roll.total;
                break;
            default:
                break;
        }

        await actor.update(update);

        // Update original card and re-display
        let replacementRoll = JSON.parse(JSON.stringify(message.roll));
        replacementRoll.dice.push(roll.dice[0]);
        replacementRoll.total += roll.total;

        const messageUpdate = {};
        messageUpdate.data = {};
        messageUpdate.content = replacementRoll.total;
        messageUpdate.id = message.id;
        messageUpdate.roll = replacementRoll;

        if (game.user.isGM) {
            await message.update(messageUpdate, {"diff": true});
            await message.setFlag('od6s', 'total', replacementRoll.total);
            if ((+messageUpdate.content) >= (message.getFlag('od6s', 'difficulty'))) {
                await message.setFlag('od6s', 'success', true);
            }
        } else {
            game.socket.emit('system.od6s', {
                operation: 'updateRollMessage',
                message: message,
                update: messageUpdate
            })
        }

        // Is this an init roll?
        if (message.getFlag('core', 'initiativeRoll')) {
            if (game.user.isGM) {
                if (game.combat !== null) {
                    const combatant = game.combat.data.combatants.find(c => c.actor.id === actor.id);
                    const update = {
                        id: combatant.id,
                        initiative: replacementRoll.total
                    }
                    await combatant.update(update);
                }
            } else {
                game.socket.emit('system.od6s', {
                    operation: "updateInitRoll",
                    message: message,
                    update: messageUpdate
                })
            }
        }
    }
}
